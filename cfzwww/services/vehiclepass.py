from cfzcore.interactors.vehicle_pass import (search_for_vehicle_pass_by_owner,
                                              fetch_all_vehicle_passes)
from ..models import DBSession
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository
from .company_data_service import CompanyService
from cfzcore.vehicle_pass import VehiclePass


class CarStickersCrudService(object):
    def __init__(self, repository=VehiclePassRepository(DBSession), company_service=CompanyService()):
        self.repository = repository
        self.company_service = company_service

    def find_stickers(self, company_id):
        return self.repository.all_by_company_id(company_id)

    def save(self, company_id, **kwargs):
        from datetime import datetime
        company = self.company_service.get_with_id(company_id)
        if self.can_add_new_vehiclepass(company) is True:
            kwargs['date_issued'] = datetime.strptime(kwargs['date_issued'], '%d/%m/%Y')
            kwargs['company'] = company
            sticker = VehiclePass(**kwargs)
            sticker.issued_by = kwargs.get('issued_by', None)
            sticker = self.repository.persist(sticker)
            return self.repository.persist(sticker)
        else:
            return None

    def update(self, **kwargs):
        from datetime import datetime
        kwargs['date_issued'] = datetime.strptime(kwargs['date_issued'], '%d/%m/%Y')
        sticker = self.repository.get(kwargs.get('id', 0))
        sticker.owner = kwargs.get('owner', sticker.owner)
        sticker.vehicle_year = kwargs.get('vehicle_year', sticker.vehicle_year)
        sticker.vehicle_model = kwargs.get('vehicle_model', sticker.vehicle_model)
        sticker.color = kwargs.get('color', sticker.color)
        sticker.license_plate = kwargs.get('license_plate', sticker.license_plate)
        sticker.sticker_number = kwargs.get('sticker_number', sticker.sticker_number)
        sticker.receipt_number = kwargs.get('receipt_number', sticker.receipt_number)
        sticker.month_expired = kwargs.get('month_expired', sticker.month_expired)
        sticker.date_issued = kwargs.get('date_issued', sticker.date_issued)
        sticker.issued_by = kwargs.get('issued_by', '')
        return self.repository.persist(sticker)

    def search_by_owner(self, owner_name):
        return search_for_vehicle_pass_by_owner(owner_name, self.repository)

    def all_vehiclepasses(self):
        return fetch_all_vehicle_passes(self.repository)

    def can_add_new_vehiclepass(self, company):
        '''Informs if we are allowed to add a new vehiclepass.'''
        vehiclepass_repository = VehiclePassRepository(DBSession)
        return company.max_vehicles > len(vehiclepass_repository.get_active_vehiclepass(company.id))
        
