from ..models import courtesypass_repository
from datetime import datetime


class CourtesypassCrudService(object):
    def __init__(self, repository=courtesypass_repository):
        self.repository = repository

    def update_courtesypass(self, courtesypass, **kwargs):
        courtesypass.person = kwargs.get('person', courtesypass.person)
        courtesypass.vehicle_year = kwargs.get('vehicle_year', courtesypass.vehicle_year)
        courtesypass.vehicle_model = kwargs.get('vehicle_model', courtesypass.vehicle_model)
        courtesypass.color = kwargs.get('vehicle_color', courtesypass.color)
        courtesypass.license_plate = kwargs.get('license_plate', courtesypass.license_plate)
        courtesypass.sticker_number = kwargs.get('sticker_number', courtesypass.sticker_number)
        courtesypass.issued_by = kwargs.get('issued_by', courtesypass.issued_by)
        courtesypass.authorized_by = kwargs.get('authorized_by', courtesypass.authorized_by)
        courtesypass.month_expired = kwargs.get('month_expired', courtesypass.month_expired)
        if kwargs.get('date_issued'):
            courtesypass.date_issued = datetime.strptime(kwargs['date_issued'], '%d/%m/%Y')
        return self.repository.persist(courtesypass)

