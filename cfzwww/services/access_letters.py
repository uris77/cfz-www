from ..models import access_letter_repository
from cfzcore.letters import (
    AccessLetter,
    AccessLetterType)
from datetime import datetime


class AccessLetterCrudService(object):

    def create(self, **kwargs):
        access_letter = AccessLetter()
        access_letter.granted_to = kwargs['granted_to']
        access_letter.expiration_date = datetime.strptime(kwargs['expiration_date'], '%d/%b/%Y')
        access_letter.vehicle_license_plate = kwargs['vehicle_license_plate']
        access_letter.processing_fee = kwargs['processing_fee']
        access_letter.authorized_by = kwargs['authorized_by']
        access_letter.issued_by = kwargs['issued_by']
        access_letter.letter_type = getattr(AccessLetterType, kwargs['letter_type'])
        access_letter = access_letter_repository.persist(access_letter)
        return access_letter

    def save(self, **kwargs):
        access_letter = self.get_access_letter(**kwargs)
        access_letter.granted_to = kwargs['granted_to']
        access_letter.expiration_date = datetime.strptime(kwargs['expiration_date'], '%d/%m/%Y')
        access_letter.vehicle_license_plate = kwargs['vehicle_license_plate']
        access_letter.processing_fee = kwargs['processing_fee']
        access_letter.authorized_by = kwargs['authorized_by']
        access_letter.issued_by = kwargs['issued_by']
        access_letter.letter_type = getattr(AccessLetterType, kwargs['letter_type'])
        access_letter = access_letter_repository.persist(access_letter)
        return access_letter

    def get_access_letter(self, **kwargs):
        id = kwargs['id']
        if id:
            return access_letter_repository.get(int(id))
        else:
            return AccessLetter()
