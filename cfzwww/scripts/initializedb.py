import os
import sys
import transaction

from ConfigParser import SafeConfigParser

from sqlalchemy import engine_from_config
from sqlalchemy.orm import configure_mappers
from sqlalchemy.schema import MetaData

from pyramid.paster import (
    get_appsettings,
    setup_logging)

from ..models import (
    DBSession,
    MyModel,
    Base)

from cfzpersistence.mappings import (
    create_db,
    do_map)


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def create_cfz_database(url):
    map_models(url)
    create_db(get_metadata(url))


def map_models(url):
    metadata = get_metadata(url)
    do_map(metadata)
    configure_mappers()


def get_metadata(url):
    return MetaData(url)


def get_url(config_file):
    parser = SafeConfigParser()
    parser.read(config_file)
    return parser.get('app:main', 'sqlalchemy.url')


def main(argv=sys.argv):
    if len(argv) != 2:
        usage(argv)
    config_uri = argv[1]
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)
    #with transaction.manager:
    #    model = MyModel(name='one', value=1)
    #    DBSession.add(model)
    create_cfz_database(get_url(config_uri))
