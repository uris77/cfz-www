class CompanySummaryPresenter(object):
    __slots__ = ["id",
                 "name",
                 "directors",
                 "registration_number",
                 "created_date",
                 "telephone_number",
                 "status",
                 "links",
                 "max_vehicles",
                 "date_sent_letter_of_acceptance"]

    def __init__(self, company, application_url):
        self.id = company.id
        self.name = company.name
        self.registration_number = company.registration_number
        self.created_date = company.created_date.strftime("%d/%b/%Y")
        self.telephone_number = getattr(company, 'telephone_number', None) 
        self.max_vehicles = company.max_vehicles
        self.status = company.status
        if company.date_sent_letter_of_acceptance:
            self.date_sent_letter_of_acceptance = company.\
                date_sent_letter_of_acceptance.strftime("%d/%b/%Y")
        else:
            self.date_sent_letter_of_acceptance = None
        self.directors = [DirectorViewModel(director) for director in company.directors]
        company_href = application_url + '/company/show/' + str(self.id)
        self.links = [{'rel': 'show', 'href': company_href}]

    def __json__(self, request):
        return {
            "id": self.id,
            "name": self.name,
            "directors": self.directors,
            "registration_number": self.registration_number,
            "created_date": self.created_date,
            "telephone_number": self.telephone_number,
            "status": self.status,
            "links": self.links,
            "max_vehicles": self.max_vehicles,
            "date_sent_letter_of_acceptance": getattr(self, "date_sent_letter_of_acceptance", None)}


class CompanyDetailsPresenter(object):
    def __init__(self, company, application_url):
        from cfzcore.interactors.company import CompanyDirectorEditor
        self.id = company.id
        self.name = company.name
        self.registration_number = company.registration_number
        self.created_date = company.created_date.strftime("%d/%b/%Y")
        self.telephone_number = company.telephone_number
        self.fax_number = company.fax_number
        self.cell_number = company.cell_number
        self.state = company.state
        self.max_vehicles = company.max_vehicles
        self.country = company.country
        self.directors = [DirectorViewModel(director) for director in company.directors]
        self.description_of_goods = company.description_of_goods
        self.date_sent_letter_of_acceptance = company.date_sent_letter_of_acceptance
        if self.date_sent_letter_of_acceptance:
            self.date_sent_letter_of_acceptance = self.\
                date_sent_letter_of_acceptance.strftime("%d/%b/%Y")
        self.shareholders = [Shareholder(shareholder) for shareholder in company.shareholders]
        self.annual_reports = [AnnualReportPresenter(report) for report in company.annual_reports]
        self.incorporation_date = company.incorporation_date.strftime("%d/%m/%Y")
        self.pretty_incorporation_date = company.incorporation_date.strftime("%d/%b/%Y")
        self.goods_categories = [GoodsCategoryPresenter(category) for category in company.goods_categories]
        self.status = company.status
        company_href = application_url + '/company/show/' + str(self.id)
        self.links = [{'rel': 'show', 'href': company_href}]

    def __json__(self, request):
        return {"id": self.id,
                "name": self.name,
                "registration_number": self.registration_number,
                "created_date": self.created_date,
                "telephone_number": self.telephone_number,
                "fax_number": self.fax_number,
                "cell_number": self.cell_number,
                "state": self.state,
                "country": self.country,
                "directors": self.directors,
                "description_of_goods": self.description_of_goods,
                "shareholders": self.shareholders,
                "annual_reports": self.annual_reports,
                "max_vehicles": self.max_vehicles,
                "incorporation_date": self.incorporation_date,
                "pretty_incorporation_date": self.pretty_incorporation_date,
                "goods_categories": self.goods_categories,
                "links": self.links,
                "status": self.status,
                "date_sent_letter_of_acceptance": self.date_sent_letter_of_acceptance}


class Shareholder(object):
    def __init__(self, shareholder):
        self.id = shareholder.id
        self.first_name = shareholder.person.first_name
        self.last_name = shareholder.person.last_name
        self.shares = shareholder.shares

    def __json__(self, request):
        return {"id": self.id,
                "first_name": self.first_name,
                "last_name": self.last_name,
                "shares": self.shares
        }


class AnnualReportPresenter(object):
    __slots__ = ("id", "date_submitted", "reporting_year")

    def __init__(self, report):
        self.id = report.id
        self.date_submitted = report.date_submitted.strftime("%d/%b/%Y")
        self.reporting_year = report.reporting_year

    def __json__(self, request):
        return {"id": self.id,
                "date_submitted": self.date_submitted,
                "reporting_year": self.reporting_year}


class AddressPresenter(object):
    __slots__ = ("id", "company_id", "line1", "line2")

    def __init__(self, company):
        address = company.address
        self.id = getattr(address, "id", None)
        self.line1 = getattr(address, "line1", None)
        self.line2 = getattr(address, "line2", None)
        self.company_id = company.id

    def __json__(self, request):
        return {"id": self.id,
                "company_id": self.company_id,
                "line1": self.line1,
                "line2": self.line2}


class ContactNumbersPresenter(object):
    __slots__ = ("id", "telephone_number", "fax_number", "cell_number")

    def __init__(self, company):
        self.id = company.id
        self.telephone_number = company.telephone_number
        self.fax_number = company.fax_number
        self.cell_number = company.cell_number

    def __json__(self, request):
        return {"id": self.id,
                "telephone_number": self.telephone_number,
                "fax_number": self.fax_number,
                "cell_number": self.cell_number}


class DirectorViewModel(object):
    __slots__ = ("id", "first_name", "last_name", 
                 "start_date", "end_date",
                 "pretty_start_date", "pretty_end_date",
                 "email", "phone1", "phone2",
                 "error")

    def __init__(self, director=None):
        from cfzcore.interactors.company import CompanyDirectorEditor
        if director:
            self.id = director.id
            self.first_name = director.person.first_name
            self.last_name = director.person.last_name
            self.email = director.person.email
            self.phone1 = director.person.phone1
            self.phone2 = director.person.phone2
            self.pretty_start_date = director.start_date.strftime("%d/%b/%Y")
            self.pretty_end_date = director.end_date.strftime("%d/%b/%Y") if director.end_date else None
            self.start_date = director.start_date.strftime("%d/%m/%Y")
            self.end_date = director.end_date.strftime("%d/%m/%Y") if director.end_date else None
            directorEditor = CompanyDirectorEditor()
        else:
            self.error = "No director available"

    def __json__(self, request):
        if getattr(self, 'error', None) is not None:
            return {"error": self.error}
        else:
            return {"id": self.id,
                    "first_name": self.first_name,
                    "last_name": self.last_name,
                    "start_date": self.start_date,
                    "email": self.email,
                    "phone1": self.phone1,
                    "phone2": self.phone2,
                    "end_date": self.end_date,
                    "pretty_start_date": self.pretty_start_date,
                    "pretty_end_date": self.pretty_end_date}


class GoodsCategoryPresenter(object):
    __slots__ = ("id", "name")

    def __init__(self, good_category):
        self.id = good_category.id
        self.name = good_category.name

    def __json__(self, request):
        return {"id": self.id,
                "name": self.name}
