from datetime import datetime


class OutletShortFormatPresenter(object):
    __slots__ = ('id', 'name', 'company_id','address_id',
                 'status', 'start_date', 'company_name',
                 'links','telephone_number', 'fax_number',
                 'address',
                 'company_link',
                 'manager')

    def __init__(self, outlet):
        self.id = outlet.id
        self.name = outlet.name
        self.company_id = outlet.company.id
        self.company_name = outlet.company.name
        self.address_id = getattr(outlet.address, "id", None)
        self.status = outlet.status
        self.start_date = datetime.strftime(outlet.start_date, "%d/%b/%Y")
        show_link = "/outlet/show/" + str(self.id)
        company_link = "/company/show/" + str(self.company_id)
        self.links = dict(show=show_link, company_link=company_link)
        self.telephone_number = getattr(outlet, 'telephone_number', None)
        self.fax_number = getattr(outlet, 'fax_number', None)
        self.manager = outlet.manager.name()
        self.address = self._address_for(outlet)

    def _address_for(self, outlet):
        _address = "Plaza: " + outlet.address.line1
        if outlet.address.line2 is not None:
            _address = _address + " | Locale: " + outlet.address.line2
        return _address

    def __json__(self, request):
        return {'id': self.id,
                'name': self.name,
                'company_id': self.company_id,
                'company_name': self.company_name,
                'status': self.status,
                'start_date': self.start_date,
                'telephone_number': self.telephone_number,
                'fax_number': self.fax_number,
                'links': self.links,
                'manager': self.manager,
                'address': self.address,
                'address_id': self.address_id}


class ContactNumberPresenter(object):
    __slots__ = ('id', 'telephone_number', 'fax_number')

    def __init__(self, outlet):
        self.id = outlet.id
        self.telephone_number = getattr(outlet, 'telephone_number', None)
        self.fax_number = getattr(outlet, 'fax_number', None)

    def __json__(self, request):
        return {'id': self.id,
                'telephone_number': self.telephone_number,
                'fax_number': self.fax_number}


class AddressPresenter(object):
    __slots__ = ('id', 'line1', 'line2', 'outlet_id')

    def __init__(self, outlet):
        self.id = outlet.id
        self.line1 = getattr(outlet.address, "line1", None)
        self.line2 = getattr(outlet.address, "line2", None)
        self.outlet_id = outlet.id

    def __json__(self, request):
        return {'id': self.id,
                'line1': self.line1,
                'line2': self.line2,
                'outlet_id': self.outlet_id}


class OutletManagerPresenter(object):
    __slots__ = ('id',
                 'first_name',
                 'last_name',
                 'outlet_id',
                 'phone1',
                 'phone2')

    def __init__(self, outlet):
        manager = outlet.manager
        self.id = getattr(manager, "id", None)
        self.outlet_id = outlet.id
        self.first_name = getattr(manager, "first_name", None)
        self.last_name = getattr(manager, "last_name", None)
        self.phone1 = getattr(manager, "phone1", None)
        self.phone2 = getattr(manager, "phone2", None)

    def __json__(self, request):
        return {'id': self.id,
                'outlet_id': self.outlet_id,
                'first_name': self.first_name,
                'last_name': self.last_name,
                'phone1': self.phone1,
                'phone2': self.phone2}


class VehiclepassPresenter(object):
    __slots__ = ('id',
                 'outlet_name',
                 'outlet_id',
                 'vehicle_year',
                 'vehicle_model',
                 'owner',
                 'color',
                 'receipt_number',
                 'license_plate',
                 'date_issued',
                 'date_issued_printable',
                 'issued_by',
                 'month_expired',
                 'sticker_number')

    def __init__(self, vehiclepass):
        self.id = vehiclepass.id
        self.outlet_name = vehiclepass.outlet.name
        self.outlet_id = vehiclepass.outlet.id
        self.vehicle_year = vehiclepass.vehicle_year 
        self.vehicle_model = vehiclepass.vehicle_model
        self.owner = vehiclepass.owner
        self.license_plate = vehiclepass.license_plate
        self.date_issued_printable = datetime.strftime(vehiclepass.date_issued, '%d/%b/%Y')
        self.date_issued = datetime.strftime(vehiclepass.date_issued, '%d/%m/%Y')
        self.month_expired = vehiclepass.month_expired
        self.sticker_number = vehiclepass.sticker_number
        self.color = vehiclepass.color
        self.receipt_number = vehiclepass.receipt_number
        self.issued_by = vehiclepass.issued_by

    def __json__(self, request):
        return {'id': self.id,
                'outlet_name': self.outlet_name,
                'outlet_id': self.outlet_id,
                'color': self.color,
                'vehicle_year': self.vehicle_year,
                'vehicle_model': self.vehicle_model,
                'owner': self.owner,
                'license_plate': self.license_plate,
                'date_issued': self.date_issued,
                'date_issued_printable': self.date_issued_printable,
                'month_expired': self.month_expired,
                'receipt_number': self.receipt_number,
                'issued_by': self.issued_by,
                'sticker_number': self.sticker_number }
