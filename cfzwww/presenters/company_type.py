class CompanyTypePresenter(object):
    __slots__ = ['id', 'name']

    def __init__(self, company_type):
        self.id = company_type.id
        self.name = company_type.name

    def __json__(self, request):
        return {
                'id': self.id,
                'name': self.name
                }
