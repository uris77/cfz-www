class FormIncompleteErrorPresenter(object):
    __slots__ = ['place_of_origin', 'telephone_number']

    def __init__(self, error):
        self.place_of_origin = error.place_of_origin
        self.telephone_number = error.telephone_number
