class CourtesypassPresenter(object):
    __slots__ = ['id', 'person', 'vehicle_year', 'vehicle_model',
                 'color', 'license_plate', 'date_issued', 'pretty_date_issued',
                 'sticker_number', 'month_expired', 'expired', 
                 'authorized_by', 'issued_by']

    def __init__(self, courtesypass):
        self.id = courtesypass.id
        self.person = courtesypass.person
        self.vehicle_year = courtesypass.vehicle_year
        self.vehicle_model = courtesypass.vehicle_model
        self.color = courtesypass.color
        self.license_plate = courtesypass.license_plate
        self.date_issued = courtesypass.date_issued.strftime('%d/%m/%Y')
        self.pretty_date_issued = courtesypass.date_issued.strftime('%d/%b/%Y')
        self.sticker_number = courtesypass.sticker_number
        self.month_expired = courtesypass.month_expired
        self.expired = courtesypass.expired
        self.authorized_by = courtesypass.authorized_by
        self.issued_by = courtesypass.issued_by

    def __json__(self, request):
        return{
                'id': self.id,
                'person': self.person,
                'vehicle_year': self.vehicle_year,
                'vehicle_model': self.vehicle_model,
                'color': self.color,
                'license_plate': self.license_plate,
                'date_issued': self.date_issued,
                'pretty_date_issued': self.pretty_date_issued,
                'sticker_number': self.sticker_number,
                'month_expired': self.month_expired,
                'expired': self.expired,
                'authorized_by': self.authorized_by,
                'issued_by': self.issued_by
            }
