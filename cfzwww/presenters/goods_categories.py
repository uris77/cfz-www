class GoodCategoryPresenter(object):
    __slots__ = ['id', 'name']

    def __init__(self, good_category):
        self.id = good_category.id
        self.name = good_category.name

    def __json__(self, request):
        return {
                'id': self.id,
                'name': self.name
                }

