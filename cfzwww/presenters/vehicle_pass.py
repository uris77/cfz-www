from datetime import datetime


class VehiclePassShortFormatPresenter(object):
    __slots__ = ("id", "company", "vehicle_year", "vehicle_model", "owner",
                 "license_plate", "date_issued", "month_expired",
                 "sticker_number", "expired", "pretty_date_issued",
                 "receipt_number", "issued_by")

    def __init__(self, vehicle_pass):
        self.id = vehicle_pass.id
        self.owner = vehicle_pass.owner
        self.company = vehicle_pass.company.name
        self.vehicle_model = vehicle_pass.vehicle_model
        self.vehicle_year = vehicle_pass.vehicle_year
        self.license_plate = vehicle_pass.license_plate
        self.date_issued = datetime.strftime(vehicle_pass.date_issued, '%d/%m/%Y')
        self.pretty_date_issued = datetime.strftime(vehicle_pass.date_issued, '%d/%b/%Y')
        self.month_expired = vehicle_pass.month_expired
        self.sticker_number = vehicle_pass.sticker_number
        self.expired = vehicle_pass.expired
        self.receipt_number = vehicle_pass.receipt_number
        self.issued_by = vehicle_pass.issued_by

    def __json__(self, request):
        return {'id': self.id,
                'owner': self.owner,
                'company': self.company,
                'vehicle_model': self.vehicle_model,
                'vehicle_year': self.vehicle_year,
                'license_plate': self.license_plate,
                'month_expired': self.month_expired,
                'sticker_number': self.sticker_number,
                'expired': self.expired,
                'pretty_date_issued': self.pretty_date_issued,
                'receipt_number': self.receipt_number,
                'issued_by': self.issued_by,
                'date_issued': self.date_issued}


class VehiclePassDetailedFormatPresenter(object):
    __slots__ = ("id", "company", "vehicle_year", "vehicle_model",
                 "color", "license_plate", "date_issued", "sticker_number",
                 "receipt_number","month_expired", "expired",
                 "pretty_date_issued")

    def __init__(self, vehicle_pass):
        self.id = vehicle_pass.id
        self.company = vehicle_pass.company.name
        self.vehicle_model = vehicle_pass.vehicle_model
        self.color = vehicle_pass.color
        self.license_plate = vehicle_pass.license_plate
        self.date_issued = datetime.strftime(vehicle_pass.date_issued, '%d/%m/%Y')
        self.pretty_date_issued = datetime.strftime(vehicle_pass.date_issued, '%d/%b/%Y')
        self.sticker_number = vehicle_pass.sticker_number
        self.receipt_number = vehicle_pass.receipt_number
        self.month_expired = vehicle_pass.month_expired
        self.expired = vehicle_pass.expired

    def __json__(self, request):
        return {'id': self.id,
                'company': self.company,
                'vehicle_model': self.vehicle_model,
                'color': self.color,
                'license_plate': self.license_plate,
                'expired': self.expired,
                'pretty_date_issued': self.pretty_date_issued,
                'date_issued': self.date_issued}
