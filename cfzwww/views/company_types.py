from pyramid.view import view_config
from ..models import company_type_repository
from ..presenters.company_type import CompanyTypePresenter

class CompanyTypeView(object):
    def __init__(self, request):
        self.request = request

    @view_config(route_name="company_types",
                 renderer="json",
                 request_method="GET")
    def company_types(self):
        '''List all company types'''
        company_types = company_type_repository.all()
        return [CompanyTypePresenter(company_type) for company_type in company_types]
