from pyramid.view import view_config

from ..models import DBSession

from cfzpersistence.repository.investor import InvestorRepository
from cfzcore import InvestorBackgroundChecker


@view_config(route_name="investor_pass_background_check",
             renderer="json")
def pass_background_check(request):
    investor_repository = InvestorRepository(DBSession)
    background_checker = InvestorBackgroundChecker(investor_repository)
    investor = background_checker.save_background_check(int(request.matchdict['id']),
                                                        'P')
    return dict(passed=investor)


@view_config(route_name="investor_fail_background_check",
             renderer="json")
def fail_background_check(request):
    investor_repository = InvestorRepository(DBSession)
    background_checker = InvestorBackgroundChecker(investor_repository)
    investor = background_checker.save_background_check(int(request.matchdict['id']),
                                                        'F')
    return dict(failed=investor)
