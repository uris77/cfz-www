from pyramid.view import view_config

from ..models import (company_repository,
                      courtesypass_repository)

from ..presenters.courtesypass import CourtesypassPresenter
from cfzcore.company.passes import CourtesyPass


@view_config(route_name="courtesypass",
             renderer="json",
             request_method="GET")
def get_courtesypass(request):
    courteyspass = courtesypass_repository.get(
        request.matchdict['courtesypass_id'])
    return CourtesypassPresenter(courtesypass)


@view_config(route_name="courtesypass",
             renderer="json",
             request_method="PUT")
def update_courtesypass(request):
    from ..services.courtesypass import CourtesypassCrudService
    courtesypass = courtesypass_repository.get(
        request.matchdict['courtesypass_id'])
    service = CourtesypassCrudService(courtesypass_repository)
    courtesypass = service.update_courtesypass(
        courtesypass,
        **request.json_body)
    return CourtesypassPresenter(courtesypass)


@view_config(
    route_name="courtesypass_new",
    renderer="json",
    request_method="POST")
def create_courtesypass(request):
    company = company_repository.get(request.matchdict['company_id'])
    request.json_body['company'] = company
    params = dict(company=company)
    params.update(request.json_body)
    params['issued_by'] = _get_user(request).username
    courtesypass = CourtesyPass.create(**params)
    courtesypass = courtesypass_repository.persist(courtesypass)
    return CourtesypassPresenter(courtesypass)


@view_config(route_name="courtesypass_list",
             renderer="json",
             request_method="GET")
def list_courtesypass(request):
    company = company_repository.get(request.matchdict['company_id'])
    courtesypasses = courtesypass_repository.all_for_company(company)
    if courtesypasses:
        _courtesypass_presenter_list(courtesypasses)
    else:
        return []


def _courtesypass_presenter_list(courtesypasses):
    return [CourtesypassPresenter(crtsypass) for crtsypass in courtesypasses]


def _get_user(request):
    from pyramid.security import authenticated_userid
    from ..models import user_repository
    user_id = authenticated_userid(request)
    return user_repository.get(user_id)
