from pyramid.view import view_config

from ..models import DBSession
from ..presenters.company import (CompanySummaryPresenter,
                                  AnnualReportPresenter,
                                  CompanyDetailsPresenter)
from ..presenters import ShareholderPresenter
from ..services.company_data_service import CompanyService

from cfzpersistence.repository.company import CompanyRepository
from cfzcore.interactors.company import CompanyShareholder


@view_config(route_name="company_index",
             permission="admin",
             renderer="templates/kuta/company/list.jinja2")
def index(request):
    return dict(page="companies", company_section="list")


@view_config(route_name="company_list",
             permission="admin",
             renderer="json")
def list_company(request):
    company_repository = CompanyRepository(DBSession)
    page = get_page(request)
    max_records = get_max_records(request)
    companies = company_repository.get_by_pagination(page, max_records)
    total_companies = company_repository.total()
    companies_list = get_companies_list(companies, request.application_url)
    total_pages = total_companies/max_records + total_companies % max_records
    return dict(companies=companies_list,
                total=total_companies,
                page=page,
                totalPages=total_pages,
                perPage=max_records)


def get_page(request):
    int(request.matchdict['page']) - 1 if 'page' in request.matchdict else 0


def get_max_records(request):
    if 'per_page' in request.matchdict:
        return int(request.matchdict['per_page'])
    else:
        return 10


def get_companies_list(companies, url):
    return [CompanyDetailsPresenter(company, url) for company in companies]


@view_config(route_name="company", permission="admin", renderer="json")
def company_api(request):
    from ..services.company_data_service import CompanyBuildService
    company_service = CompanyService()
    if request.method == 'GET':
        _company = company_service.get_with_id(int(request.matchdict['id']))
        company = CompanyDetailsPresenter(_company, request.application_url)
        return company
    elif request.method == 'POST':
        company_build_service = CompanyBuildService()
        company = company_build_service.create(**request.json_body)
        return CompanyDetailsPresenter(company, request.application_url)
    else:
        print("wrong request")
        return dict(error="Wrong Request")
    return dict(company="company")


@view_config(route_name="company",
             renderer="json",
             request_method="PUT")
def update_company(request):
    from ..services import company_crudservice
    data_sent = request.json_body
    company_crudservice.update(request.matchdict['id'], **data_sent)
    return dict()


@view_config(route_name="search_for_company_with_name",
             renderer="json")
def search_for_company_with_name(request):
    repository = CompanyRepository(DBSession)
    companies = repository.search_for(request.matchdict['company_name'])
    companies_list = [CompanySummaryPresenter(comp) for comp in companies]
    return companies_list


@view_config(route_name='show_company',
             permission='admin',
             renderer='templates/kuta/company/show.jinja2')
def show_company(request):
    company_id = request.matchdict['id']
    company_service = CompanyService()
    company = company_service.get_with_id(company_id)
    return dict(page="companies", company=company.name)


@view_config(route_name="save_company_contact_numbers",
             renderer="json")
def save_company_contact_numbers(request):
    from ..presenters.company import ContactNumbersPresenter
    company_service = CompanyService()
    if request.method == 'GET':
        company = company_service.get_with_id(int(request.matchdict['id']))
        return ContactNumbersPresenter(company)
    elif request.method == 'PUT':
        contact_numbers = request.json_body
        _company = company_service.update_contact_numbers(
            request.matchdict['id'],
            **contact_numbers)
        return ContactNumbersPresenter(_company)
    else:
        return dict(error="Wrong Request!")


@view_config(route_name="company_address",
             renderer="json")
def save_company_address(request):
    from ..presenters.company import AddressPresenter
    company_service = CompanyService()
    if request.method == 'GET':
        company = company_service.get_with_id(int(request.matchdict['id']))
        return AddressPresenter(company)
    elif request.method == 'PUT':
        _company = company_service.update_address(**request.json_body)
        return AddressPresenter(_company)
    elif request.method == 'POST':
        _company = company_service.create_address(**request.json_body)
        return AddressPresenter(_company)
    else:
        return dict(error="Wrong Request")


@view_config(route_name="save_company_director",
             renderer="json")
def save_company_director(request):
    from ..presenters.company import DirectorViewModel
    company_service = CompanyService()
    director = company_service.save_director(
        request.matchdict['id'], **request.json_body)
    return DirectorViewModel(director)


# ANNUAL REPORTS
@view_config(route_name="save_company_annual_report", renderer='json')
def save_company_annual_report(request):
    from ..services.annual_reports import AnnualReportCrudService
    crud_service = AnnualReportCrudService()
    submitted_data = request.json_body
    annual_report = crud_service.create_annual_report(request.matchdict['id'],
                                                      **submitted_data)
    return AnnualReportPresenter(annual_report)


@view_config(route_name="company_annual_reports", renderer='json')
def annual_reports_list(request):
    repository = CompanyRepository(DBSession)
    annual_reports = repository.annual_reports_for_company(
        int(request.matchdict['id']))
    reports = [AnnualReportPresenter(report) for report in annual_reports]
    return reports


# SHAREHOLDERS
@view_config(route_name="save_company_shareholder", renderer="json")
def save_company_shareholder(request):
    from cfzcore.shareholder import Shareholder
    from ..services.shareholders import ShareholderCrudService
    crud_service = ShareholderCrudService()
    repository = CompanyRepository(DBSession)
    company = repository.get(request.matchdict['id'])
    if request.method == 'POST':
        shareholder = Shareholder(**request.json_body)
        company.shareholders.append(shareholder)
        repository.persist(company)
        return ShareholderPresenter(shareholder)
    if request.method == 'PUT':
        print "crud_service: ", crud_service.__dict__
        shareholder = crud_service.update(**request.json_body)
        return ShareholderPresenter(shareholder)


@view_config(route_name="company_shareholders", renderer="json")
def shareholders_list(request):
    repository = CompanyRepository(DBSession)
    company_shareholder = CompanyShareholder(int(request.matchdict['id']),
                                             repository)
    _shareholders = company_shareholder.all_shareholders()
    return [ShareholderPresenter(shareholder) for shareholder in _shareholders]


@view_config(route_name="save_company_date_delivered_letter_of_acceptance",
             renderer="json")
def save_company_date_delivered_letter_of_acceptance(request):
    company_service = CompanyService()
    date_letter_sent = request.json_body['date_sent_letter_of_acceptance']
    company = company_service.save_date_letter_of_acceptance_was_delivered(
        request.matchdict['id'], date_letter_sent)
    return CompanySummaryPresenter(company, request.application_url)


@view_config(route_name="company_form",
             renderer="templates/kuta/company/form.jinja2")
def company_form(request):
    return dict(page='companies',
                company_section='create')


@view_config(route_name="company_selectbox",
             renderer="templates/company/selectbox.jinja2")
def selectbox(request):
    companies = CompanyRepository(DBSession).all_authorized_companies()
    return dict(companies=companies)


@view_config(route_name="company_directors",
             renderer="json")
def directors(request):
    from ..presenters.company import DirectorViewModel
    company_id = request.matchdict.get('id', None)
    if company_id:
        company_service = CompanyService()
        directors = company_service.get_directors_for_company_with(company_id)
        return directors
    else:
        return DirectorViewModel()


@view_config(route_name='vehiclepass_maxnumbers', renderer='json')
def update_vehiclepass_max_number(request):
    company_service = CompanyService()
    company = company_service.update_max_vehiclepass(
        request.matchdict['id'],
        request.json_body['max_vehicles'])
    return CompanySummaryPresenter(company, request.application_url)
