var pending_applications_module = angular.module("pending_applications_module", ["ngResource", "ui.directives"]);
pending_applications_module.config(function($routeProvider){
   $routeProvider.
      when('/', {templateUrl: '/static/partials/application/pending_applications.html'}).
      otherwise({redirectTo: '/',
                 template: '/static/partials/application/pending_applications.html'});
});

pending_applications_module.service('pending_applications_service', function($rootScope, $http){

   this.applications = {};
   
   this.get_pending_applications = function(){
      $http({method:'GET',
             url:'/applications/pending/list'}).
         success(function(data, status){
            this.applications = angular.copy(data);
            $rootScope.$broadcast("applications_retrieved");
         });
   }

   this.approve = function(id){
      $http({method:'POST',
             url:'/applications/approve/'+id}).
         success(function(data,status){
            $("#messages").empty();
            $("#messages").append(data);
      });
   }

   this.deny = function(id, reason_for_denial){
      $http({method:'POST',
             url:'/applications/deny/' + id, 
            data:{'id': id, 'reason_for_denial': reason_for_denial}}).
            success(function(data, status){
               $("#messages").empty();
               $("#messages").append(data);
               $rootScope.$broadcast("application_denied");
      });
   }
});

function PendingApplicationsController($scope, $http, pending_applications_service){

   $scope.applications = {};
   $scope.show_deny_modal = false;
   $scope.show_empty_reason_for_denial_message = false;

   $scope.$on('applications_retrieved', function(){
      $scope.applications = angular.copy(applications);
   });

   $scope.$on("application_denied", function(){
      $scope.close_deny_form();
   });

   $scope.show_deny_form = function(application){
      $scope.application = angular.copy(application);
      $scope.show_deny_modal = true;
   }

   $scope.deny_application = function(application){
      if(CFZ.is_present(application.reason_for_denial)){
         pending_applications_service.deny(application.id, application.reason_for_denial);
         $scope.show_deny_modal = false;
      }else{
         $scope.show_empty_reason_for_denial_message = true;
      }
   }

   $scope.approve_application = function(application){
      pending_applications_service.approve(application.id);
   }

   $scope.close_deny_form = function(){
      console.log("closing form");
      $scope.show_deny_modal = false;
   }

   pending_applications_service.get_pending_applications();
}

