define([
       'jquery',
       'lodash',
       'backbone',
       'app/vehicle_pass/vehiclepass-model',
       'vehiclePassModule',
], function($, _, backbone, VehiclePass, vehiclePassModule){
      VehiclePassModule = vehiclePassModule.VehiclePassModule;
      VehiclePassModule.VehiclepassSearchCollection = Backbone.Collection.extend({
         url: function(){
            return "/vehicle_pass/search" + this.searchTerm;
         }
      },{
         search: function(searchTerm){
            var results = new VehiclePassModule.VehiclepassSearchCollection();
            results.searchTerm = searchTerm;
            results.fetch({
               success: function(){
                  VehiclePassModule.vent.trigger("search:results", results);
               },
               error: function(collection, response){
                  VehiclePassModule.vent.trigger("search:error", response);
               }
            });
         }
      });


      return new VehiclePassModule.VehiclepassSearchCollection;
});


