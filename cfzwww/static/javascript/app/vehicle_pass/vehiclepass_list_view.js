define([
       'jquery',
       'lodash',
       'backbone',
       'app/vehicle_pass/vehiclepass_collections',
       'vehiclePassModule',
       'text!templates/vehiclepass/list.html'
], function($, _, backbone, VehiclepassCollection, vehiclePassModule, vehiclepassListTemplate){
      
      VehiclePassModule = vehiclePassModule.VehiclePassModule;
      VehiclePassModule.searchEvents = _.extend({}, Backbone.Events);
      VehiclePassModule.vent = _.extend({}, Backbone.Events);

      VehiclePassModule.VehiclepassSearchCollection = Backbone.Collection.extend({
         url: function(){
            return "/vehicle_pass/search/" + this.searchTerm;
         }
      },{
         search: function(searchTerm){
            var results = new VehiclePassModule.VehiclepassSearchCollection();
            results.searchTerm = searchTerm;
            results.fetch({
               success: function(){
                  var resultsData = {
                     vehiclepasses: results.models,
                     _: _
                  };
                  VehiclePassModule.searchEvents.trigger("search:results", resultsData);
               },
               error: function(collection, response){
                  VehiclePassModule.vent.trigger("search:error", response);
               }
            });
         }
      });


      VehiclePassModule.VehiclepassSearchFormView = Backbone.View.extend({
         events: {
            'keyup .search-query': 'searchVehiclePassesForOwner'
         },

         searchVehiclePassesForOwner: _.debounce(function(e){
            var ownerName = e.target.value.trim();
            if(_.isEmpty(ownerName)){
               this.render();
            }else{
               VehiclePassModule.VehiclepassSearchCollection.search(ownerName);
            }
         }, 500)

      });


      VehiclePassModule.VehiclepassListView = Backbone.View.extend({
         initialize: function(){
            this.vehiclepasses = VehiclepassCollection;
            VehiclePassModule.searchEvents.bind("search:results", this.renderSearchResults, this);
         },

         render: function(){
            this.vehiclepasses.fetch({success: function(passes){
               var data = {
                  vehiclepasses: passes.models,
                  _: _
               };
               var compiledTemplate = _.template(vehiclepassListTemplate, data);
               $("#mainpanel").html(compiledTemplate);
            }});
         },

         renderSearchResults: function(vehiclepassList){
            console.log(vehiclepassList.vehiclepasses);
            var compiledTemplate = _.template(vehiclepassListTemplate, vehiclepassList);
            $("#mainpanel").html(compiledTemplate);
         }


      });


      var vehiclePassListView =  new VehiclePassModule.VehiclepassListView({el: $("#mainpanel")});


      return vehiclePassListView;
});
