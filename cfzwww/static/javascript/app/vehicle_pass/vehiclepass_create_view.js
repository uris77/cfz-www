define([
       'jquery',
       'lodash',
       'jqueryui',
       'backbone',
       'vehiclePassModule',
       'app/vehicle_pass/vehiclepass-model',
       'text!templates/vehiclepass/create.html'
], function($, _, jqui, backbone, vehiclePassModule, VehiclePass, vehiclepassCreateTemplate){

   VehiclePassModule = vehiclePassModule.VehiclePassModule;
   VehiclePassModule.vent = _.extend({}, Backbone.Events);
   
   VehiclePassModule.VehiclePassCreateView = Backbone.View.extend({
      el: "#mainpanel",
      model: VehiclePass,
      initialize: function(){
      },

      showError: function(model, error){
         alert("error");
      },
      events: {
         'submit #vehicleform': 'submit',
         'click #clear_vehicle_form': 'clear'
      },
      submit: function(e){
         e.preventDefault();
         vehiclePass = new VehiclePass();
         vehiclePass.set(
            {'owner':            $('input[name=owner]').val(),
             'company_id':       $('select option:selected').val(),
             'vehicle_year':     $('input[name=vehicle_year]').val(),
             'vehicle_model':    $('input[name=vehicle_model]').val(),
             'color':            $('input[name=color]').val(),
             'license_plate':    $('input[name=license_plate]').val(),
             'sticker_number':   $('input[name=sticker_number]').val(),
             'date_issued':      $('input[name=date_issued]').val(),
             'month_expired':    $('input[name=month_expired]').val(),
             'receipt_number':   $('input[name=receipt_number]').val()
            }
         );
         vehiclePass.save(null, {
            success: VehiclePassModule.vent.trigger("vehicle_pass:created")
         });
         //vehiclePass.bind('error', this.showError, this);
      },

      clear: function(e){
         //e.preventDefault();
         this.render();
      },

      render: function(){
         var compiledTemplate = _.template(vehiclepassCreateTemplate);
         $.get("/company/selectbox", function(data){
            $("#companies-selectbox").empty();
            $("#companies-selectbox").append(data);
         });
         //$("#mainpanel").html(compiledTemplate);
         this.$el.html(compiledTemplate);
         $("#date_issued").datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
         });
      }
   });

   createView =  new VehiclePassModule.VehiclePassCreateView()

   VehiclePassModule.vent.on("vehicle_pass:created", function(){
      var success_message = "<div class='alert span12'>Vehicle Pass Created!</div>";
      createView.clear();
      $("#success_message").append(success_message);
   });

   return createView;

});
