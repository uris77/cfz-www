(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['contact_table'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<h4>Outlet Contact Numbers</h4>\n<div id=\"alert-space\">\n</div>\n\n<table class=\"display table table-striped table-bordered\">\n   <thead>\n      <tr role=\"row\">\n         <th role=\"Telephone #\">Telephone #</th>\n         <th role=\"Fax #\">Fax #</th>\n         <th/>\n      </tr>\n   </thead>\n   <tbody>\n      <td>";
  if (stack1 = helpers.telephone_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.telephone_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n      <td>";
  if (stack1 = helpers.fax_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.fax_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n      <td>\n         <button class=\"btn btn-primary\" id=\"edit-btn\" title=\"Save Outlet Contact Numbers\">\n            Edit\n         </button>\n      </td>\n   </tbody>\n</table>\n\n";
  return buffer;
  });
})();