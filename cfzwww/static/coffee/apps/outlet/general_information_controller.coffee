@Outlet.module "GeneralInformation.Controller", (Controller, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API = 
		show: ->
			basicView = new App.GeneralInformation.BasicView({model: App.outlet})
			App.mainRegion.show(basicView)

	App.vent.on "generalInformation:show", ->
		API.show()

	App.GeneralInformation.on 'start', ->
		API.show()
