@Outlet.module "Address", (Address, App, Backbone, Marionette, $, _)->
  @startWithParent = false

  class Address.Controller extends  Marionette.Controller
    initialize: (options={}) ->
      @region = options.region
      @address = options.address
      @showView()
      App.vent.on "outlet:address:edit:form", => @showForm()
      App.vent.on "outlet:address:form:cancel", => @showTable()
      App.commands.addHandler "outlet:address:save", (address, attributes) => 
        @saveAddress(address, attributes)

    getFormView: ->
      new Address.Views.Form
        model: @address

    getTableView: ->
      new Address.Views.Table
        model: @address

    showForm: ->
      @currentView?.close()
      @currentView = @getFormView()
      @show @currentView

    showTable: ->
      @currentView?.close()
      @currentView = @getTableView()
      @show @currentView

    show: (view) ->
      @listenTo view, "close", @close
      @region.show view

    showView: ->
      if @hasAddress()
        @showTable()
      else
        @showForm()

    hasAddress: ->
      !!@address?.get('line1') || @address?.get('line2')

    saveMessage: ->
      "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button>Saved Outlet's Address!</div>"

    errorMessage: ->
      "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button>Error while saving Outlet's Address!</div>"

    saveAddress: (address, attributes) ->
      savingAddress = address.save(attributes)
      savingAddress.done (_address) =>
        App.contactNumber = _address
        @showTable()
        $('#alert-space').html @saveMessage()
      savingAddress.fail (jqXHR) =>
        console.warn "Error saving address: ", jqXHR
        @errorMessage()

  Address.on "start", ->
    @controller = new Address.Controller
      region:         App.mainRegion
      address: App.address
