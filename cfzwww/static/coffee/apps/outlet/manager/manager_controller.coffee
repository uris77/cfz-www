@Outlet.module "Manager", (Manager, App, Backbone, Marionette, $, _)->
	@startWithParent = false

 class Manager.Controller extends  Marionette.Controller
    initialize: (options={}) ->
      @region = options.region
      @manager = options.manager
      @showView()
      App.vent.on "outlet:manager:edit:form", => @showForm()
      App.vent.on "outlet:manager:form:cancel", => @showTable()
      App.commands.addHandler "outlet:manager:save", (manager, attributes) => 
        @saveManager(manager, attributes)

    getFormView: ->
      new Manager.Views.Form
        model: @manager

    getTableView: ->
      new Manager.Views.Table
        model: @manager

    showForm: ->
      @currentView?.close()
      @currentView = @getFormView()
      @show @currentView

    showTable: ->
      @currentView?.close()
      @currentView = @getTableView()
      @show @currentView

    show: (view) ->
      @listenTo view, "close", @close
      @region.show view

    showView: ->
      if @hasManager()
        @showTable()
      else
        @showForm()

    hasManager: ->
      !!@manager?.get('first_name') && @manager?.get('last_name')

    saveMessage: ->
      "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button>Saved Outlet's Manager!</div>"

    errorMessage: ->
      "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button>Error while saving Outlet's Manager!</div>"

    saveManager: (manager, attributes) ->
      savingManager = manager.save(attributes)
      savingManager.done (_manager) =>
        App.manager = _manager
        @showTable()
        $('#alert-space').html @saveMessage()
      savingManager.fail (jqXHR) =>
        console.warn "Error saving manager: ", jqXHR
        @errorMessage()

  Manager.on "start", ->
    @controller = new Manager.Controller
      region:   App.mainRegion
      manager:  App.manager
