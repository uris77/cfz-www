// Generated by CoffeeScript 1.6.3
this.Outlet = (function(Backbone, Marionette) {
  var App;
  App = new Marionette.Application;
  App.addRegions({
    mainRegion: '#tab-content',
    navBarRegion: '#outlet-navbar'
  });
  App.addInitializer(function() {
    return App.module("OutletEntities").start();
  });
  App.vent.on("entity:initialized", function() {
    App.outlet = App.request("outlet:get:entity");
    App.module("GeneralInformation").start();
    App.module('Navbar').start();
  });
  App.vent.on("contactNumbers:request:show", function() {
    App.execute("fetchContactNumbers", App.outlet.get('id'));
  });
  App.vent.on("address:request:show", function() {
    App.execute("fetchAddress", App.outlet.get('id'));
  });
  App.vent.on("manager:request:show", function() {
    App.execute("fetchManager", App.outlet.get('id'));
  });
  App.vent.on("contactNumbers:initialized", function() {
    App.module("ContactNumber").stop();
    App.contactNumbers = App.request("outlet:contactNumbers:entity");
    return App.module('ContactNumber').start();
  });
  App.vent.on("outletAddress:initialized", function() {
    App.module('Address').stop();
    App.address = App.request("outlet:address:entity");
    return App.module('Address').start();
  });
  App.vent.on("outletManager:initialized", function() {
    App.module('Manager').stop();
    App.manager = App.request("outlet:manager:entity");
    return App.module('Manager').start();
  });
  App.on("initialize:after", function() {
    if (Backbone.history) {
      return Backbone.history.start();
    }
  });
  return App;
})(Backbone, Marionette);
