@Outlet = do (Backbone, Marionette) ->
	App = new Marionette.Application
	App.addRegions
		mainRegion: '#tab-content'
		navBarRegion: '#outlet-navbar'

	App.addInitializer ->
		App.module("OutletEntities").start()

	App.vent.on "entity:initialized", ->
		App.outlet = App.request "outlet:get:entity"
		App.module("GeneralInformation").start()
		App.module('Navbar').start()
		return

	App.vent.on "contactNumbers:request:show", ->
		App.execute "fetchContactNumbers", App.outlet.get('id')
		return

	App.vent.on "address:request:show", ->
		App.execute "fetchAddress", App.outlet.get('id')
		return

	App.vent.on "manager:request:show", ->
		App.execute "fetchManager", App.outlet.get('id')
		return

	App.vent.on "contactNumbers:initialized", ->
		App.module("ContactNumber").stop()
		App.contactNumbers = App.request "outlet:contactNumbers:entity"
		App.module('ContactNumber').start()

	App.vent.on "outletAddress:initialized", ->
		App.module('Address').stop()
		App.address = App.request "outlet:address:entity"
		App.module('Address').start()

	App.vent.on "outletManager:initialized", ->
		App.module('Manager').stop()
		App.manager = App.request "outlet:manager:entity"
		App.module('Manager').start()

	App.on "initialize:after", ->
		if Backbone.history
			Backbone.history.start()

	App
