@Outlet.module "OutletEntities", (OutletEntities, App, Backbone, Marionette, $, _) ->

	class OutletEntities.Outlet extends Backbone.Model
		url: ->
			"/outlet/#{@get('id')}"

		defaults:
			start_date: ''
			links: ''

	API =
		fetchOutlet: ->
			locationpath = location.pathname.split('/')
			outlet_id = locationpath[locationpath.length - 1]
			API.outlet = new OutletEntities.Outlet()
			API.outlet.set('id', outlet_id)
			API.outlet.fetch().done(->
				App.vent.trigger "entity:initialized"
			)

		getOutlet: ->
			API.outlet

	App.reqres.addHandler "outlet:get:entity", ->
		API.getOutlet()

	OutletEntities.on "start", ->
		API.fetchOutlet()

