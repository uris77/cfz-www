@GoodsCategories.module "GoodsCategories.Views", (Views, Application, Backbone, Marionette, $, _ ) ->
	@startWithParent = false

	class Views.NoGoodsCategory extends Marionette.ItemView
		template: Handlebars.templates['goods_category_empty']

	class Views.CreateForm extends Marionette.ItemView
		template: Handlebars.templates['goods_category_form']

		events:
			'click #save-btn': 'save'
			'keyup #category-name': 'enableButton'

		onShow: ->
			$('#save-btn').attr('disabled', 'disabled')

		save: (domEvent)->
			domEvent.preventDefault()
			@model.set 'name', $('#category-name').val()
			Application.commands.execute "goodsCategories:add", @model
			$('#category-name').val('')

		enableButton: (domEvent)->
			if(_.isEmpty($(domEvent.target).val().trim()))
				$('#save-btn').attr('disabled', 'disabled')
			else
				$('#save-btn').removeAttr 'disabled'

	class Views.GoodCategory extends Marionette.ItemView
		template: Handlebars.templates['table_body']
		tagName: 'tr'

		events:
			'click [data-delete-name]': 'deleteGoodCategory'

		deleteGoodCategory: (domEvent) ->
			domEvent.preventDefault()
			if confirm "Are you sure you want to delete #{@model.get('name')} ?" then @model.destroy() else false

		onShow:  =>
			name = @model.get('name')
			$("a[name=#{name}]").editable
				value: name
			$("a[name=#{name}]").on('save', (e, params)=>
				@model.set('name',params.newValue) 
				Application.commands.execute "goodsCategories:save", @model
			)


	class Views.Table extends Marionette.CompositeView
		template: Handlebars.templates['table_header']
		itemViewContainer: 'tbody'
		itemView: Views.GoodCategory
