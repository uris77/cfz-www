(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['goods_category_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<form class=\"well form-inline\">\n   <fieldset>\n      <input type='text' name='name' id='category-name' placeholder='Add Goods category'></input>\n      <button class='btn btn-primary' id='save-btn'>\n         <i class='icon-ok'></i>Save\n      </button>\n   </fieldset>\n</form>\n";
  });
})();