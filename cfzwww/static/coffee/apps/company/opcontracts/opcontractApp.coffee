@Company.module "OperationalContract", (OperationalContract, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API =
		show: (company, operationContracts, generalInformationView)->
			@opcontractsLayout = @createOpcontractsLayout()
			generalInformationView.opcontracts.show @opcontractsLayout
			contractsList = new App.OperationalContract.Views.List(collection: operationContracts)
			contractsCreateBtn = new App.OperationalContract.Views.CreateButton
			@opcontractsLayout.contractsRegion.show contractsList
			@opcontractsLayout.createBtnRegion.show contractsCreateBtn

		createOpcontractsLayout: ->
			layout = new App.OperationalContract.Views.Layout()
			layout

		addOpContractToCollection: (opcontract, collection)->
			collection.add opcontract
			collection

	App.commands.addHandler "opcontracts:show", (opcontracts, generalInformationView)->
		API.show App.company, opcontracts, generalInformationView

	App.commands.addHandler "company:operationalcontract:save", (operationalContract)->
		App.vent.trigger "spinner:start"
		promise = operationalContract.save()
		promise.done((data, textStatus, jqXHR)->
			bootbox.alert("<h4 class='alert alert-success alert-block'>Saved Operation Contract!</h4>", ->
				App.vent.trigger "spinner:stop"
				newContract = new App.Entities.OperationalContract data
				API.addOpContractToCollection newContract, App.operationContracts
			)
		)
		promise.fail((options)=>
			console.log "Failure saving opcontract: ", options
			bootbox.alert("<h4 class='alert alert-error alert-block'>Failed to save Operation Contract!</h4>", ->
				App.vent.trigger "spinner:stop"
			)
		)

