@Company.module "CompanyAddress", (CompanyAddress, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	class CompanyAddress.Controller extends Marionette.Controller 
		initialize: (options) ->
			if options.address.get('id')
				@showAddressInfo options.address
			else
				@showAddressForm(options.address)
			App.vent.on "address:model:save", (attributes, address) => 
				@saveAddress(attributes, address)
			App.vent.on "address:edit:form", (address) => @showAddressForm(address)
			App.vent.on "address:form:cancel", (address) => @showAddressInfo(address)

		show: (view)->
			App.mainRegion.show(view)

		showAddressInfo: (address) ->
			addressView = @getView()
			@show addressView

		showAddressForm: (address)->
			formView = @getFormView()
			@show formView

		getView: ->
			new CompanyAddress.Show.AddressTable
				model: App.address

		getFormView: ->
			new CompanyAddress.Show.EmptyAddressView
				model: App.address

		saveMessage: ->
			"<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button>Saved Company's Address!</div>"

		saveAddress: (attributes, address) ->
			address.save(attributes,{
				success: (model, response, options)=>
					App.address = address
					@showAddressInfo address
					$('#alert-space').html @saveMessage()
				error: (model, xhr, options)->
					alert("Could not save Address!")
			})


	App.vent.on "company:address:show", ->
		new CompanyAddress.Controller
			address: App.address
