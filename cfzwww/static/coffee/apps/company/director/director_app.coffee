@Company.module "CompanyDirector", (CompanyDirector, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API = 
		show: ->
			view = @getListView()
			App.mainRegion.show(view)

		showListView: ->
			view = @getListView()
			App.mainRegion.show(view)

		showNewForm: (director)->
			App.mainRegion.show(new CompanyDirector.List.DirectorFormView(model: director))

		getListView: ->
			new CompanyDirector.List.DirectorCollectionView
				collection: App.directors

	App.vent.on "company:directors:list", ->
		API.showListView()

	App.vent.on "company:show:newDirectorForm", ->
		director = new App.Entities.CompanyDirector()
		director.set('companyId', App.company.get('id'))
		API.showNewForm(director)

	App.vent.on "company:show:editDirectorForm", (director)->
		director.set('companyId', App.company.get('id'))
		API.showNewForm(director)
