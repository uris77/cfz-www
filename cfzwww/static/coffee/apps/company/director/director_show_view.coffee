@Company.module "CompanyDirector.List", (List, App, Backbone, Marionette, $, _)->

	class List.DirectorView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['directors_list']
		tagName: 'tr'

		events:
			'click #edit-director-btn': 'editDirector'

		editDirector: ->
			App.vent.trigger "company:show:editDirectorForm", @model


	class List.DirectorFormView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['director_form']

		events:
			'click #cancel-btn' : 'cancelDirectorForm'
			'click #save-btn' : 'saveDirector'
			'blur #first-name' : 'requiredFieldAlert'
			'blur #last-name': 'requiredFieldAlert'
			'change #start-date': 'requiredFieldAlert'


		onRender: ->
			_.defer((args)->
				model = args[0]
				$('#first-name-alert').hide()
				$('#last-name-alert').hide()
				$('#start-date-alert').hide()
				$("#start-date").attr("readonly", "true")

				if _.isUndefined(model.get('id'))
					$('#save-btn').attr('disabled', 'disabled')
					$("#start-date").datepicker
						format: "dd/mm/yyyy"
						startDate: '01/01/1994'
						showWeek: true
						firstDay: 1
						changeMonth: true
						changeYear: true
						showOtherMonths: true
					.on('changeDate', (ev)->
						$(@).change()
					)

				$('#end-date').datepicker
					format: "dd/mm/yyyy"
					startDate: '01/01/1994'
					endDate: ''
					showWeek: true
					firstDay: 1
					changeMonth: true
					changeYear: true
					showOtherMonths: true
				.on('show', ->
					if _.isUndefined(model.get('id'))
						$(@).val('01/01/1994').datepicker('update')
				)

			, [@model])


		initialize: ->
			@validationErrors = ['first-name', 'last-name']


		saveDirector: (e)->
			e.preventDefault()
			first_name = $('#first-name').val()
			last_name = $('#last-name').val()
			start_date = $('#start-date').val()
			email = $('#email').val()
			phone1 = $('#director-phone1').val()
			phone2 = $('#director-phone2').val()
			@model.set {
				'first_name': first_name,
				'last_name': last_name,
				'email': email,
				'phone1': phone1,
				'phone2': phone2,
				'start_date': start_date
			}
			if !_.isEmpty($('#end-date').val())
				@model.set('end_date', $('#end-date').val())
			@model.save(null,
				{success: (model, response, options)->
					bootbox.alert("<h4>Director saved successfully!</h4>", ->
						App.vent.trigger "directors:request:show"
					)
				})

		cancelDirectorForm: (e)->
			e.preventDefault()
			App.vent.trigger "directors:request:show"

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-btn').attr("disabled", "disabled")
			else
				$('#save-btn').removeAttr("disabled")
