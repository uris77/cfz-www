@Company.module "AnnualReport", (AnnualReport, App, Backbone, Marionette, $, _)->

	class AnnualReport.AnnualReportView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['annualreport_list']
		tagName: 'tr'


	class AnnualReport.AnnualReportsCollectionView extends Backbone.Marionette.CompositeView
		itemView: AnnualReport.AnnualReportView
		itemViewContainer: 'tbody'
		template: Handlebars.templates['annualreport_header']

		events: 
			'click #add-report-btn': 'showForm'

		showForm: ->
			App.vent.trigger 'company:show:annualReportNewForm'


	class AnnualReport.AnnualReportFormView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['annualreport_form']

		events:
			'click #cancel-report-btn': 'cancelForm'
			'keyup #report-submitted-date': 'requiredFieldAlert'
			'keyup #reporting-year': 'requiredFieldAlert'
			'click #save-report-btn': 'saveAnnualReport'

		onRender: ->
			_.defer(->
				$('#report-submitted-date-alert').hide()
				$('#reporting-year-alert').hide()
				$('#report-submitted-date').attr("readonly", "true")
				$('#save-report-btn').attr("disabled", "disabled")

				$('#report-submitted-date').datepicker
						showWeek: true
						firstDay: 1
						changeMonth: true
						changeYear: true
						showOtherMonths: true
						format: "dd/mm/yyyy"
						startDate: moment(new Date()).format("DD/MM/YYYY")
					.on('changeDate', (ev)->
						$(@).keyup()
					)
			)

		initialize: ->
			@validationErrors = []

		cancelForm: (e)->
			e.preventDefault()
			App.vent.trigger 'reports:request:show'

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-report-btn').attr("disabled", "disabled")
			else
				$('#save-report-btn').removeAttr("disabled")

		saveAnnualReport: (e)->
			e.preventDefault()
			submitted_date = $('#report-submitted-date').val()
			reporting_year = $('#reporting-year').val()
			@model.save({
				date_submitted: submitted_date,
				reporting_year: reporting_year},
				{success: (model, response, options)->
					bootbox.alert('Annual Report Saved!', ->
						App.vent.trigger 'reports:request:show'
					)

				})
