@Company.module "AnnualReport", (AnnualReport, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API =
		showListView: ->
			view = @getListView()
			App.mainRegion.show(view)

		getListView: ->
			new AnnualReport.AnnualReportsCollectionView
				collection: App.reports

		showForm: (report) ->
			App.mainRegion.show(new AnnualReport.AnnualReportFormView(model: report))

	App.vent.on "company:reports:list", ->
		API.showListView()

	App.vent.on "company:show:annualReportNewForm", ->
		report = new App.Entities.AnnualReport()
		report.set('companyId', App.company.get('id'))
		API.showForm(report)
