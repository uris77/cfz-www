@Company.module "Shareholder", (Shareholder, App, Backbone, Marionette, $, _)->

	class Shareholder.ShareholderView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['shareholders_list']
		tagName: 'tr'

		events:
			'click #edit-shareholder-btn': 'editShareholder'

		editShareholder: ->
			App.vent.trigger 'company:show:shareholderForm', @model

	class Shareholder.ShareholderCollectionView extends Backbone.Marionette.CompositeView
		itemView: Shareholder.ShareholderView
		itemViewContainer: 'tbody'
		template: Handlebars.templates['shareholders_header']

		events:
			'click #add-shareholder-btn': 'showForm'

		showForm: ->
			App.vent.trigger 'company:show:shareholderNewForm'


	class Shareholder.ShareholderFormView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['shareholders_form']

		events:
			'click #cancel-shareholder-btn': 'cancelForm'
			'blur #shareholder-first-name': 'requiredFieldAlert'
			'blur #shareholder-last-name': 'requiredFieldAlert'
			'blur #shares': 'requiredFieldAlert'
			'click #save-shareholder-btn': 'saveShareholder'

		onRender: ->
			_.defer(->
				$('#shareholder-first-name-alert').hide()
				$('#shareholder-last-name-alert').hide()
				$('#shares-alert').hide()
				$('#save-shareholder-btn').attr('disabled', 'disabled')
			)

		initialize: ->
			@validationErrors = ['shareholder-first-name', 'shareholder-last-name', 'shares']

		cancelForm: (e)->
			e.preventDefault()
			App.vent.trigger "shareholders:request:show"

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-shareholder-btn').attr("disabled", "disabled")
			else
				$('#save-shareholder-btn').removeAttr("disabled")

		saveShareholder: (e)->
			e.preventDefault()
			first_name = $('#shareholder-first-name').val()
			last_name = $('#shareholder-last-name').val()
			shares = $('#shares').val()
			@model.save({
				first_name: first_name,
				last_name: last_name,
				shares: shares},
				{success: (model, response, options)->
					bootbox.alert('Shareholder Saved!', ->
						App.vent.trigger "shareholders:request:show"
					)

				})

