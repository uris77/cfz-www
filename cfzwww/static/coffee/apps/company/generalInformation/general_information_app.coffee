@Company.module "GeneralInformationApp", (GeneralInformationApp, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API =
		show: (company, authorizedpersons)->
			@generalInformationView = @_createGeneralInformationView company
			console.log "generalInformationView: ", @generalInformationView
			App.mainRegion.show @generalInformationView
			@authorizedPersonsLayout = @_createAuthorizedPersonsView()
			@generalInformationView.authorizedPersons.show @authorizedPersonsLayout
			listView = new App.AuthorizedPersons.Views.List
						collection: authorizedpersons
			createBtnView = new App.AuthorizedPersons.Views.CreateButton
			@authorizedPersonsLayout.authorizedPersonsListRegion.show listView
			@authorizedPersonsLayout.createBtnRegion.show createBtnView

		_createAuthorizedPersonsView: ->
			layout = new App.AuthorizedPersons.Views.Layout
			layout

		_createGeneralInformationView: (company)->
			@generalInformationView = new GeneralInformationApp.Show.GeneralInformationView 
										model: company
			@generalInformationView

		getAuthorizedPersonsLayout: ->
			@authorizedPersonsLayout

		getGeneralInformationView: ->
			@generalInformationView

		fetchCompany: ->
			locationpath = location.pathname.split('/')
			companyId = locationpath[locationpath.length - 1]
			API.company = new App.Entities.Company()
			API.company.set('id', companyId)
			API.company.fetch()
			return API.company

		showAuthorizedPersonsForm: (model, representationtypes)->
			formView = new App.AuthorizedPersons.Views.Form {model: model, representationtypes: representationtypes}
			@getGeneralInformationView().authorizedPersons.show formView

		showAuthorizedPersonsList: (authorizedpersons)->
			listView = new App.AuthorizedPersons.Views.List collection: authorizedpersons
			createBtnView = new App.AuthorizedPersons.Views.CreateButton
			@getGeneralInformationView().authorizedPersons.show @getAuthorizedPersonsLayout()
			@getAuthorizedPersonsLayout().authorizedPersonsListRegion.show listView
			@getAuthorizedPersonsLayout().createBtnRegion.show createBtnView

		saveAuthorizedPerson: (authorizedperson)->
			authorizedperson.save()

		addAuthorizedPersonToCollection: (authorizedperson, collection)->
			collection.add authorizedperson
			collection

	App.vent.on "generalInformation:show", ->
		API.show App.company, App.authorizedpersons, App.operationContracts
		if App.operationContracts
			App.commands.execute "company:opcontracts:show"

	App.commands.addHandler "company:opcontracts:show", ->
		App.commands.execute "opcontracts:show", App.operationContracts, API.getGeneralInformationView()

	App.vent.on "company:authorizedperson:showForm", (model)->
		API.showAuthorizedPersonsForm model, App.representationtypes

	App.vent.on "company:authorizedpersons:showList", (authorizedpersons)->
		API.showAuthorizedPersonsList authorizedpersons

	App.commands.addHandler "company:authorizedperson:save", (authorizedperson)->
		App.vent.trigger "spinner:start"
		promise = API.saveAuthorizedPerson authorizedperson
		promise.done((data, textStatus, jqXHR)->
			bootbox.alert("<h4 class='alert alert-success alert-block'>Saved Authorized Person!</h4>", ->
				App.vent.trigger "spinner:stop"
				newAuthorizedPerson = new App.Entities.AuthorizedPerson data
				authorizedpersons = API.addAuthorizedPersonToCollection newAuthorizedPerson, App.authorizedpersons
				API.showAuthorizedPersonsList authorizedpersons
			)
		)
		promise.fail((options)=>
			bootbox.alert("<h4 class='alert alert-error alert-block'>Failed to save Authorized Person!</h4>", ->
				App.vent.trigger "spinner:stop"
				API.showAuthorizedPersonsList App.authorizedpersons
			)
		)

	GeneralInformationApp.on 'start', ->
		API.show App.company, App.authorizedpersons, App.operationContracts
		fetchedAuthorizedPersons = App.reqres.request "company:authorizedpersons:fetch", App.company
		fetchedAuthorizedPersons.done (authorizedpersons)->
			App.authorizedpersons = authorizedpersons
			API.showAuthorizedPersonsList App.authorizedpersons
