@Company.module "GeneralInformationApp.Show", (Show, App, Backbone, Marionette, $, _)->

	class Show.AcceptanceLetterDate extends Marionette.ItemView
		template: Handlebars.templates['acceptance_date']
		tagName: 'p'


	class Show.AcceptanceLetterDateForm extends Marionette.ItemView
		template: Handlebars.templates['acceptance_date_form']

		onRender: ->
			_.defer(->
				$( "#acceptance-date-letter" ).datepicker
					showWeek: true
					firstDay: 1
					changeMonth: true
					changeYear: true
					dateFormat: "dd/mm/yyyy"
			) 

		events: 
			'click #save-acceptance-date-btn': 'saveAcceptanceDate'

		initialize: ->
			console.log "Showing AcceptanceDateForm.... with template: ", Handlebars.templates['acceptance_date_form'] 

		saveAcceptanceDate: (e)->
			e.preventDefault()
			@model.save({date_sent_letter_of_acceptance: $('#acceptance-date-letter').val()},{
				success: (model, response, options)->
					bootbox.alert("<h4>Acceptance Letter Saved!</h4>", ->
						App.vent.trigger "company:acceptance_letter")
				error: ->
					bootbox.alert("<h4>Acceptance Letter Could Not Be Saved!</h4>")
				})

	class Show.GeneralInformationView extends Marionette.Layout
		className: 'clearfix alert-block span12'
		template: Handlebars.templates['company_general_information']
		regions:
			letterForm: '#date-acceptance-letter-form'
			acceptanceDate: '#acceptance-date'
			authorizedPersons: '#authorized-persons-region'
			opcontracts: '#opcontracts-region'

		onRender: ->
			if @model.get('date_sent_letter_of_acceptance')
				@letterForm.show(new Show.AcceptanceLetterDate({model: @model}))
			else
				console.log "No Acceptance Date set yet..."
				acceptanceLetter  = new App.Entities.AcceptanceLetter()
				acceptanceLetter.set('id', @model.get('id'))
				@letterForm.show(new Show.AcceptanceLetterDateForm({model: acceptanceLetter}))

			$('#generalInfo-tab-item').toggleClass('active', true)

		onShow: ->
			$('#status').editable
				value: @model.get 'status'
				source: [
					{value: 'Operational', text: 'Operational'},
					{value: 'Not Operational', text: 'Not Operational'},
					{value: 'Off Balance Sheet', text: 'Off Balance Sheet'}
				]

			$('#status').on('save', (e, params)=>
				@model.set('status',params.newValue) 
				@model.save() 
			)

		initialize: ->
			App.vent.on "company:acceptance_letter", @showAcceptanceDate, @
			console.log "Starting GeneralInformationView with template: ", @template 

		showAcceptanceDate: ->
			@letterForm.show(new Show.AcceptanceLetterDate({model: @model}))
