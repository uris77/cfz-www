@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

	class Entities.CourtesyPass extends Backbone.Model
		urlRoot: ->
			"/company/#{@get('company_id')}/courtesypass"

		defaults:
			date_issued: moment(new Date()).format("DD/MM/YYYY")
			owner: ''
			color: ''
			vehicle_year: ''
			vehicle_model: ''
			license_plate: ''
			month_expired: ''
			sticker_number: ''
			issued_by: ''
			authorized_by: ''


	class Entities.CourtesyPassList extends Backbone.Collection
		model: Entities.CourtesyPass
		url: ->
			"/company/#{@company.id}/courtesypass_list"

		initialize: (company)->
			@company = company

	API =
		fetchAll: (company)->
			@courtesypasses = new Entities.CourtesyPassList(company)
			@courtesypasses.fetch().done(->
				App.vent.trigger "company:courtesypasses:initialized"
			)

		getAll: ->
			@courtesypasses

		addCourtesypass: (courtesypass)->
			@courtesypasses.add courtesypass, merge: true 
			@courtesypasses


	App.vent.on "company:courtesypasses:initialize", (company)->
		API.fetchAll(company)

	App.reqres.addHandler "courtesypasses:all", ->
		API.getAll()

	App.commands.addHandler "courtesypasses:add", (courtesypass)->
		API.addCourtesypass courtesypass

