@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

	class Entities.VehicleSticker extends Backbone.Model
		url: ->
			"/company/#{@get('companyId')}/sticker"

		defaults:
			date_issued: moment(new Date()).format("DD/MM/YYYY")
			owner: ''
			color: ''
			vehicle_year: ''
			vehicle_model: ''
			license_plate: ''
			month_expired: ''
			receipt_number: ''
			issued_by: ''
			sticker_number: ''



	class Entities.VehicleStickers extends Backbone.Collection
		model: Entities.VehicleSticker
		url: ->
			"/company/#{@company.id}/stickers"

		initialize: (company)->
			@company = company


	class Entities.MaxVehicleStickers extends Backbone.Model
		url: ->
			"/company/#{@get('id')}/max_vehiclepass"

		defaults:
			max_vehicles: 6

	API =
		fetchAll: (company)->
			API.vehicle_stickers = new Entities.VehicleStickers(company)
			API.vehicle_stickers.fetch().done(->
				App.vent.trigger "company:stickers:initialized"
			)

		getAll: ->
			API.vehicle_stickers

		getActivePasses: (vehiclepasses)->
			(vehiclepass for vehiclepass in vehiclepasses.models when vehiclepass?.get('expired') is false)

		addMaximumVehiclepass: (vehiclepass, max_vehicles)->
			if max_vehicles >= @getActivePasses(@getAll()).length
				vehiclepass.save({
					max_vehicles: max_vehicles},
					{success: (model, response, options)->
						bootbox.alert('<h4>Number of Stickers has been Updated!</h4>', ->
							_company = App.reqres.request 'company:entity'
							_company.set('max_vehicles', model.get('max_vehicles'))
							App.commands.execute 'company:update', _company
						)
					})
			else
				bootbox.alert('<h4>Can not exceed number of vehicle passes assigned to this company!</h4>')


	App.reqres.addHandler "company:stickers:entities", ->
		API.getAll()

	App.commands.addHandler "fetchVehicleStickers", (company)->
		API.fetchAll(company)

	App.commands.addHandler "company:add:vehiclepass", (vehiclepass, max_vehicles)->
		API.addMaximumVehiclepass vehiclepass, max_vehicles

	App.reqres.addHandler "vehiclepass:not_expired:list", (vehiclepass_list)->
		API.getActivePasses vehiclepass_list
