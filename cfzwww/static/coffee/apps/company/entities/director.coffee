@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

	class Entities.CompanyDirector extends Backbone.Model
		url: ->
			"/company/#{@get('companyId')}/director"

		defaults:
			first_name: ''
			last_name: ''
			start_date: moment(new Date()).format("DD/MM/YYYY")
			email: ''
			phone1: ''
			phone2: ''


	class Entities.CompanyDirectors extends Backbone.Collection
		model: Entities.CompanyDirector

		url: ->
			"/company/#{@company.id}/directors"

		initialize: (company)->
			@company = company

	API = 
		fetch: (companyId) ->
			API.director = new Entities.CompanyDirector
			API.director.set('company_id', companyId)
			API.director.fetch().done(->
				App.vent.trigger "director:initialized"
			)

		get: ->
			return API.address

		fetchAll: (company) ->
			API.directors = new Entities.CompanyDirectors(company)
			API.directors.fetch().done(->
				App.vent.trigger( "companyDirectors:initialized")
			)

		getAll: ->
			return API.directors

	App.reqres.addHandler "company:director:entity", ->
		API.get()

	App.commands.addHandler "fetchDirector", (companyId)->
		API.fetch(companyId)

	App.reqres.addHandler "company:directors:entities", ->
		API.getAll()

	App.commands.addHandler "fetchDirectors", (company)->
		API.fetchAll(company)

