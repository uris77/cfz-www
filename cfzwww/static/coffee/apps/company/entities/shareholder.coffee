@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

	class Entities.Shareholder extends Backbone.Model
		url: ->
			"/company/#{@get('companyId')}/shareholder"

		defaults:
			first_name: ''
			last_name: ''
			shares: '0'


	class Entities.Shareholders extends Backbone.Collection
		model: Entities.Shareholder

		url: ->
			"/company/#{@company.id}/shareholders"

		initialize: (company)->
			@company = company

	API =
		fetchAll: (company)->
			API.shareholders = new Entities.Shareholders(company)
			API.shareholders.fetch().done(->
				App.vent.trigger "company:shareholders:initialized"
			)

		getAll: ->
			API.shareholders

	App.reqres.addHandler "company:shareholders:entities", ->
		API.getAll()

	App.commands.addHandler "fetchShareholders", (company)->
		API.fetchAll(company)
