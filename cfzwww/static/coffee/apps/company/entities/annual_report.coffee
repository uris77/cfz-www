@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
	class Entities.AnnualReport extends Backbone.Model
		url: ->
			"/company/#{@get('companyId')}/annual_report"

		defaults:
			submitted_date: moment(new Date()).format("DD/MM/YYYY")

	class Entities.AnnualReports extends Backbone.Collection
		url: ->
			"/company/#{@company.id}/annual_reports"

		initialize: (company) ->
			@company = company

	API =
		fetchAll: (company)->
			API.reports = new Entities.AnnualReports(company)
			API.reports.fetch().done(->
				App.vent.trigger "company:reports:initialized"
			)

		getAll: ->
			API.reports

	App.reqres.addHandler "company:reports:entities", ->
		API.getAll()

	App.commands.addHandler "fetchAnnualReports", (company)->
		API.fetchAll(company)
