@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

	class Entities.OperationalContract extends Backbone.Model
		url: ->
			"/company/#{@get('company_id')}/operational_contract"

	class Entities.OperationalContracts extends Backbone.Collection
		model: Entities.OperationalContract
		url: ->
			"/company/#{@company.id}/operational_contracts"

		initialize: (options)->
			@company = options.company

	API =
		fetchAll: (company)->
			defer = $.Deferred()
			@contracts = new Entities.OperationalContracts(company: company)
			@contracts.fetch
				success: =>
					defer.resolve(@contracts)
			defer.promise()

	App.reqres.addHandler "company:operationalContracts:fetch", (company)->
		API.fetchAll company
