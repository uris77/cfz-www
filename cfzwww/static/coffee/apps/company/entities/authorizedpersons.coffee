@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

	class Entities.RepresentationType extends Backbone.Model
		url: ->
			"/representation_type"
		defaults:
			name: ''


	class Entities.RepresentationTypes extends Backbone.Collection
		model: Entities.RepresentationType
		url: ->
			"/representation_types"


	class Entities.AuthorizedPerson extends Backbone.Model
		urlRoot: ->
			"/company/#{@get('company_id')}/authorizedrepresentative"

	class Entities.AuthorizedPersons extends Backbone.Collection
		url: ->
			"/company/#{@company.id}/authorizedrepresentatives_list"

		initialize: (options)->
			@company = options.company


	API =
		fetchAll: ->
			defer = $.Deferred()
			@representationtypes = new Entities.RepresentationTypes()
			@representationtypes.fetch
				success: =>
					defer.resolve(@representationtypes)
			defer.promise()


	AuthorizedPersonAPI =
		fetchAll: (company)->
			defer = $.Deferred()
			@authorizedpersons = new Entities.AuthorizedPersons(company: company)
			@authorizedpersons.fetch
				success: =>
					defer.resolve(@authorizedpersons)
			defer.promise()


	App.reqres.addHandler "company:representationtypes:fetch", ->
		API.fetchAll()

	App.reqres.addHandler "company:authorizedpersons:fetch", (company)->
		AuthorizedPersonAPI.fetchAll company
