@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

	class Entities.CompanyAddress extends Backbone.Model
		url:  ->
			"/company/#{@attributes.company_id}/address"

	API = 
		fetch: (companyId) ->
			API.address = new Entities.CompanyAddress()
			API.address.set('company_id', companyId)
			API.address.fetch().done(->
				App.vent.trigger "companyAddress:initialized"
			)

		get: ->
			return API.address

	App.reqres.addHandler "company:address:entity", ->
		API.get()

	App.commands.addHandler "fetchAddress", (companyId)->
		API.fetch(companyId)
