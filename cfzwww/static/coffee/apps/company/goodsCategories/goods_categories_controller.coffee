@Company.module "GoodsCategories.Controller", (Controller, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API =
		show: (company)->
			@companyGoodsCategories = App.reqres.request "company:goodsCategories:all"
			_goodsCategories = App.reqres.request "goodsCategories:all" 
			@goodsCategories = new App.GoodsCategories.Entities.GoodsCategories(goodsCategory for goodsCategory in _goodsCategories.models when parseInt(goodsCategory.get('id')) not in @getCompanyGoodsCategoriesIds(@companyGoodsCategories))
			layout = @createLayout(@goodsCategories, @companyGoodsCategories)
			App.mainRegion.show(@layout)
			selectOptionCollectionView = new App.GoodsCategories.SelectOptionCollectionView
												collection: @goodsCategories
			layout.goodsSelectRegion.show selectOptionCollectionView
			companyGoodsCategoriesView = new App.GoodsCategories.CollectionView
												collection: @companyGoodsCategories
												itemView: App.GoodsCategories.ItemView
			layout.listRegion.show companyGoodsCategoriesView

		createLayout: (goodsCategories, companyGoodsCategories)->
			@layout = new App.GoodsCategories.Layout
				goods_categories: goodsCategories
				company_goods_categories: companyGoodsCategories
			@layout

		getLayout: ->
			@layout

		getCompanyGoodsCategories: ->
			@companyGoodsCategories

		getCompanyGoodsCategoriesIds: (companyGoodsCategories)->
			parseInt(category.get('id')) for category in companyGoodsCategories.models

		setCompanyGoodsCategories: (companyGoodsCategories)->
			@companyGoodsCategories = companyGoodsCategories

		castToCompanyGoodsCategory: (category)->
			company = App.reqres.request "company:entity"
			companyGoodsCategory = new App.GoodsCategories.Entities.CompanyGoodsCategory({company: company})
			companyGoodsCategory.set({'id': category.get('id')})
			companyGoodsCategory.set({'name': category.get('name')})
			companyGoodsCategory

		castToGoodsCategory: (companyCategory)->
			goodsCategory = new App.GoodsCategories.Entities.GoodsCategory
			goodsCategory.set
				id: companyCategory.get('id')
				name: companyCategory.get('name')
			goodsCategory

		getCompany: ->
			App.reqres.request "company:entity"


		addCategoryToCompany: (category, company)->
			if _.isUndefined(@getCompanyGoodsCategories())
				company = App.reqres.request "company:entity"
				@companyGoodsCategories = new App.Entities.CompanyGoodsCategories({company: company})
				@companyGoodsCategories.add(category)
			else
				companyCategory = @castToCompanyGoodsCategory(category)
				@getCompanyGoodsCategories().add(companyCategory)
				@saveCategory companyCategory

		saveCategory: (category)->
			promise = category.save()
			promise.done((category)=>
				bootbox.alert("<h4 class='alert alert-success alert-block'>Assigned category to company!</h4>", ->
					console.log "saved category: ", category
				)
			)
			promise.fail((options)=>
				bootbox.alert("<h4 class='alert alert-error alert-block'>Failed to assign category to company!</h4>", ->
					console.log "Failed to save category: ", options.statusText
				)
			)

		removeCategoryFromCompany: (category, company)->
			@companyGoodsCategories.remove(category)
			categoryCopy = @castToGoodsCategory(category)
			@goodsCategories.push(categoryCopy)
			category.setCompany(company)
			promise = category.destroy()
			promise.done( (category)=>
				bootbox.alert("<h4 class='alert alert-success alert-block'>Removed category from company!</h4>", ->
					console.log 'removed category: '
				)
			)
			promise.fail( (options)->
				bootbox.alert("<h4 class='alert alert-error alert-block'>Failed to remove category from company!</h4>", ->
					console.log 'Failed to remove category from company :', options.statusText
				)
			)

	App.commands.addHandler "company:goodsCategories:show", (company)->
		API.show(company)

	App.commands.addHandler "company:goodsCategory:add", (category)->
		App.vent.trigger "spinner:start"
		API.addCategoryToCompany(category, API.getCompany())
		collectionView = new App.GoodsCategories.CollectionView
			collection: API.getCompanyGoodsCategories()
			itemView: App.GoodsCategories.ItemView
		API.getLayout().listRegion.show(collectionView)
		App.vent.trigger "spinner:stop"

	App.commands.addHandler "company:goodsCategories:remove", (category)->
		API.removeCategoryFromCompany category, API.getCompany()
