@Company.module "GoodsCategories", (GoodsCategories, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	class GoodsCategories.SelectOptionItemView extends Marionette.ItemView
		template: Handlebars.templates['goods_options_view']
		tagName: "option"
		attributes: ->
			{"value": @model.get('id')}

	class GoodsCategories.SelectOptionCollectionView extends Marionette.CollectionView
		tagName: "select"
		id: "goods-categories-select"
		itemView: GoodsCategories.SelectOptionItemView

	class GoodsCategories.ItemView extends Marionette.ItemView
		tagName: 'li'
		template: Handlebars.templates['company_goods_category']

		events:
			'click .close': 'removeCategory'

		removeCategory: ->
			App.commands.execute 'company:goodsCategories:remove', @model

	class GoodsCategories.NoItemsView extends Marionette.ItemView
		template: Handlebars.templates['company_no_goods_category']

	class GoodsCategories.CollectionView extends Marionette.CollectionView
		tagName: 'ul'
		className: 'no-style, span6'
		itemView: GoodsCategories.Itemview
		emptyView: GoodsCategories.NoItemsView

	class GoodsCategories.Layout extends Marionette.Layout
		template: Handlebars.templates['goods_categories_layout']
		regions:
			formRegion: "#goods-form-region"
			listRegion: "#goods-list-region"
			goodsSelectRegion: '#goods-select-region'

		events:
			'click #goods-categories-btn': 'addCategory'
			'change #goods-categories-select': 'selectCategory'

		initialize: (options)->
			@company_goods_categories = options.company_goods_categories
			@goods_categories = options.goods_categories

		addCategory: (e)->
			e.preventDefault()
			if @isCategory(@getSelectedCategory())
				@removeCategory(@getSelectedCategory())
				@addToCompany(@getSelectedCategory())

		selectCategory: (e)->
			e.preventDefault()
			selectBoxEl = @getSelectBoxEl()
			selectedId = selectBoxEl.options[selectBoxEl.selectedIndex].value
			selectedCategory = category for category in @goods_categories.models when category.get('id')==parseInt(selectedId)
			@setSelectedCategory(selectedCategory)

		removeCategory: (category)->
			@goods_categories.remove(category)

		isCategory: (category)->
			category.get('id') != -1

		setSelectedCategory: (category)->
			@_selectedCategory = category

		getSelectedCategory: ->
			@_selectedCategory

		getSelectBoxEl: ->
			document.getElementById("goods-categories-select")

		unselectOptions: ->
			@getSelectBoxEl().value = -1

		addToCompany: (category)->
			App.commands.execute "company:goodsCategory:add", category

