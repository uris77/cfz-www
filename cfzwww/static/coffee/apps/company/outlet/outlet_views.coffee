@Company.module "Outlet", (Outlet, App, Backbone, Marionette, $, _)->

	class Outlet.OutletView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['outlet_list']
		tagName: 'tr'

		events:
			'click #show-outlet-btn': 'showOutlet'

		showOutlet: (e)->
			e.preventDefault()
			window.location = @model.get('links').show


	class Outlet.OutletsCollectionView extends Backbone.Marionette.CompositeView
		template: Handlebars.templates['outlet_header']
		itemView: Outlet.OutletView
		itemViewContainer: 'tbody'

	class Outlet.OutletLayout extends Backbone.Marionette.Layout
		template: Handlebars.templates['outlet_layout']

		regions:
			btnRegion: '#outlet-create-region'
			listRegion: '#outlet-list-region'

	class Outlet.CreateOutletButtonView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['outlet_create_button']

		initialize: ->
			@model.set('createUrl', @createUrl())

		events:
			'click #add-outlet-btn': 'addOutlet'

		addOutlet: (e)->
			e.preventDefault()
			console.log "triggering company:show:outletForm"
			App.vent.trigger 'company:show:outletForm', @model

		createUrl: ->
			"/company/#{@model.id}/outlet/create"


	class Outlet.CreateOutletView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['outlet_form']

		events:
			'click #save-outlet-btn' : 'saveOutlet'
			'click #cancel-outlet-btn' : 'cancelForm'
			'keyup #outlet-name' : 'requiredFieldAlert'
			'keyup #start-date' : 'requiredFieldAlert'
			'keyup #outlet-first-name' : 'requiredFieldAlert'
			'keyup #outlet-last-name' : 'requiredFieldAlert'
			'keyup #outlet-telephone' : 'requiredFieldAlert'
			'keyup #outlet-plaza' : 'requiredFieldAlert'
			'keyup #outlet-locale' : 'requiredFieldAlert'

		initialize: ->
			App.vent.on "company:save:error", @showErrors
			@validationErrors = ['outlet-name',
								 'outlet-last-name',
								 'outlet-first-name',
								 'outlet-telephone',
								 'outlet-plaza',
								 'outlet-locale']

		hideAlertMessages: ->
			$('#outlet-name-alert').hide()
			$('#start-date-alert').hide()

		onRender: ->
			_.defer(->
				$("#outlet-start-date").datepicker
					showWeek: true
					firstDay: 1
					changeMonth: true
					changeYear: true
					format: "dd/mm/yyyy"
				.on('changeDate', (ev)->
					$(@).change()
				)
					
				$('#outlet-start-date-alert').hide()
				$('#save-outlet-btn').attr("disabled", "disabled")
			) 


		saveOutlet: (e)->
			e.preventDefault()
			name = $('#outlet-name').val()
			start_date = $('#outlet-start-date').val()
			status = $('#outlet-status').val()
			line1 = $('#outlet-plaza').val()
			line2 = $('#outlet-locale').val()
			first_name = $('#outlet-first-name').val()
			last_name = $('#outlet-last-name').val()
			telephone_number = $('#outlet-telephone').val()
			fax_number = $('#outlet-fax').val()
			@model.save({
				name: name,
				line1: line1,
				line2: line2,
				first_name: first_name,
				last_name: last_name,
				telephone_number: telephone_number,
				fax_number: fax_number,
				status: status,
				start_date: start_date},
				{success: (model, response, options)->
					bootbox.alert("Outlet Saved", ->
						App.vent.trigger "outlets:request:show", model
					)
				})

		cancelForm: (e)->
			e.preventDefault()
			App.vent.trigger "outlets:request:show"

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				if e.target.id in @validationErrors
					@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-outlet-btn').attr("disabled", "disabled")
			else
				$('#save-outlet-btn').removeAttr("disabled")
			console.log "validationErrors: ", @validationErrors

