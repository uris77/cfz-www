@Company.module "Outlet", (Outlet, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API =
		showListView: ->
			outletLayout = new Outlet.OutletLayout()
			btns = new Outlet.CreateOutletButtonView({model: App.company})
			outletsView = new Outlet.OutletsCollectionView
				collection: App.outlets
				company: App.company
			outletLayout.render()
			App.mainRegion.show(outletLayout)
			outletLayout.listRegion.show(outletsView)
			outletLayout.btnRegion.show(btns)
			return outletLayout

		showForm: (outlet) ->
			App.mainRegion.show(new Outlet.CreateOutletView(model: outlet))

	App.vent.on "company:outlets:list", ->
		API.showListView()

	App.vent.on "company:show:outletForm", ->
		outlet = new App.Entities.Outlet()
		outlet.set('companyId', App.company.get('id'))
		console.log "outlet: ", outlet
		API.showForm(outlet)

	# App.vent.on "company:show:shareholderForm", (shareholder)->
	# 	shareholder.set('companyId', App.company.get('id'))
	# 	API.showForm(shareholder)
