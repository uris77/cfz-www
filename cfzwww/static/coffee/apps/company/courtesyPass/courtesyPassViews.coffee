@Company.module "CourtesyPass.Views", (Views, App, Backbone, Marionette, $, _)->

	class Views.CourtesyPassLayout extends Marionette.Layout
		template: Handlebars.templates['courtesy_pass_layout']

		regions:
			'createBtnRegion': '#create-courtesypass-btn'
			'courtesyPassListRegion': '#courtesypass-list'


	class Views.CourtesyPassRow extends Marionette.ItemView
		template: Handlebars.templates['courtesypass_list']
		tagName: 'tr'

		events:
			'click #edit-courtesypass-btn': 'editCourtesypass'

		editCourtesypass: ->
			App.commands.execute "company:show:courtesypassForm", @model


	class Views.NoCourtesyPass extends Marionette.ItemView
		template: Handlebars.templates['courtesypass_empty']
		tagName: 'tr'


	class Views.CollectionView extends Marionette.CompositeView
		template: Handlebars.templates['courtesypass_header']
		itemViewContainer: 'tbody'
		itemView: Views.CourtesyPassRow
		emptyView: Views.NoCourtesyPass


	class Views.CreateButton extends Marionette.ItemView
		template: Handlebars.templates['courtesypass_create_btn']
		tagName: 'span'

		events:
			'click #add-courtesypass-btn': 'showForm'

		showForm: ->
			_company_id = App.company.get('id')
			model = new App.Entities.CourtesyPass()
			model.set('company_id', App.company.get('id'))
			courtesypassForm = new Views.Form(model: model)
			App.mainRegion.show courtesypassForm



	class Views.Form extends Backbone.Marionette.ItemView
		template: Handlebars.templates['courtesypass_form']

		events: 
			'click #cancel-sticker-btn': 'cancelForm'
			'click #save-sticker-btn': 'saveCourtesyPass'
			'keyup #person': 'requiredFieldAlert'
			'keyup #vehicle-model': 'requiredFieldAlert'
			'keyup #vehicle-year': 'requiredFieldAlert'
			'keyup #vehicle-color': 'requiredFieldAlert'
			'keyup #license-plate': 'requiredFieldAlert'
			'keyup #sticker-number': 'requiredFieldAlert'
			'keyup #authorized-by': 'requiredFieldAlert'
			'change #date-issued': 'requiredFieldAlert'

		onRender: ->
			_.defer((args)->
				model = args[0]
				if _.isUndefined(model.get('id'))
					$('#save-sticker-btn').attr("disabled", "disabled")
				else
					$('#person-alert').hide()
					$('#vehicle-model-alert').hide()
					$('#vehicle-year-alert').hide()
					$('#vehicle-color-alert').hide()
					$('#license-plate-alert').hide()
					$('#sticker-number-alert').hide()
					$('#issued-by-alert').hide()
					$('#authorized-by-alert').hide()
					$('#date-issued-alert').hide()
					$('#month-expired').val model.get('month_expired')

			, [@model])

		onShow: ->
			$("#date-issued").datepicker
				showWeek: true
				firstDay: 1
				changeMonth: true
				changeYear: true
				showOtherMonths: true
				format: "dd/mm/yyyy"
				startDate: "01/01/1994"
			.on('changeDate', (ev)->
				$(@).change()
			)

			if @model.get('issued_by')
				$('#issued-by').show()
			else
				$('#issued-by').hide()

		initialize: ->
			@validationErrors = []

		cancelForm: (e)->
			e.preventDefault()
			App.showCourtesyPass()

		saveCourtesyPass: (e)->
			e.preventDefault()
			person = $('#person').val()
			vehicle_year = $('#vehicle-year').val()
			vehicle_model = $('#vehicle-model').val()
			vehicle_color = $('#vehicle-color').val()
			license_plate = $('#license-plate').val()
			sticker_number = $('#sticker-number').val()
			date_issued = $('#date-issued').val()
			authorized_by = $('#authorized-by').val()
			month_expired = $('#month-expired').val()
			@model.set
				person: person,
				vehicle_year: vehicle_year,
				vehicle_model: vehicle_model,
				vehicle_color: vehicle_color,
				license_plate: license_plate,
				sticker_number: sticker_number,
				date_issued: date_issued,
				authorized_by: authorized_by,
				month_expired: month_expired
			App.commands.execute "company:courtesyPass:save", @model

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-sticker-btn').attr("disabled", "disabled")
			else
				$('#save-sticker-btn').removeAttr("disabled")

