@Company.module "AuthorizedPersons.Views", (Views, App, Backbone, Marionette, $, _)->

	class Views.Layout extends Marionette.Layout
		template : Handlebars.templates['authorizedpersons_layout']

		regions:
			'createBtnRegion': '#create-authorizedperson-btn'
			'authorizedPersonsListRegion': '#authorizedpersons-list'


	class Views.AuthorizedPersonsRow extends Marionette.ItemView
		template: Handlebars.templates['authorizedpersons_list']
		tagName: 'tr'
		events:
			'click #remove-btn': 'removeAuthorizedPerson'

		removeAuthorizedPerson: ->
			bootbox.confirm("<h3>Are you sure you want to delete #{@model.get('first_name')} #{@model.get('last_name')}?</h3>", (result)=>
				if result
					@model.destroy()
			)
			

	class Views.NoAuthorizedPersons extends Marionette.ItemView
		template: Handlebars.templates['authorizedpersons_empty']
		tagName: 'tr'


	class Views.CreateButton extends Marionette.ItemView
		template: Handlebars.templates['authorizedpersons_createbtn']
		tagName: 'span'

		events:
			'click #add-authorizedperson-btn': 'showForm'

		showForm: ->
			emptyModel = new App.Entities.AuthorizedPerson
			emptyModel.set('company_id', App.company.get('id'))
			App.vent.trigger "company:authorizedperson:showForm", emptyModel


	class Views.List extends Marionette.CompositeView
		template: Handlebars.templates['authorizedpersons_header']
		itemViewContainer: 'tbody'
		itemView: Views.AuthorizedPersonsRow
		emptyView: Views.NoAuthorizedPersons


	class Views.Form extends Marionette.ItemView
		template: Handlebars.templates['authorizedpersons_form']

		events:
			'click #cancel-authorizedperson-btn': 'cancelForm'
			'click #save-authorizedperson-btn': 'saveAuthorizedPerson'
			'keyup #first-name': 'requiredFieldAlert'
			'keyup #last-name': 'requiredFieldAlert'

		initialize: (options)->
			@representationtypes = options.representationtypes
			@setupValidationErrors()

		cancelForm: (e)->
			e.preventDefault()
			App.vent.trigger "company:authorizedpersons:showList", App.authorizedpersons

		saveAuthorizedPerson: (e)->
			e.preventDefault()
			first_name = $('#first-name').val()
			last_name = $('#last-name').val()
			representation_type = $('#representation-type').val()
			@model.set
				first_name: first_name
				last_name: last_name
				representation_type: representation_type
			App.commands.execute "company:authorizedperson:save", @model

		setupValidationErrors: ->
			@validationErrors = []
			if _.isUndefined(@model.get('id'))
				@validationErrors.push('first-name')
				@validationErrors.push('last-name')

		onShow: ->
			for representationtype in @representationtypes.models
				$('#representation-type').append($('<option></option>').attr("value", representationtype.get('id')).text(representationtype.get('name')))

			if _.isUndefined @model.get('id')
				$('#save-authorizedperson-btn').attr('disabled', 'disabled')

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				if indexOfId
					@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-authorizedperson-btn').attr("disabled", "disabled")
			else
				$('#save-authorizedperson-btn').removeAttr("disabled")
