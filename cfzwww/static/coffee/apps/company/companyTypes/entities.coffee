@Company.module "Entities", (Entities, Application, Backbone, Marionette, $, _)->
	@startWithParent = false

	class Entities.CompanyType extends Backbone.Model
		defaults:
			name: ''

	class Entities.CompanyTypes extends Backbone.Collection
		model: Entities.CompanyType
		url: '/company_types'

	API =
		fetchCompanyTypes: ->
			defer = $.Deferred()
			@companyTypes = new Entities.CompanyTypes()
			@companyTypes.fetch
				success: =>
					defer.resolve(@companyTypes)
			defer.promise()


	Application.reqres.addHandler "companyTypes:fetch", ->
		API.fetchCompanyTypes()

