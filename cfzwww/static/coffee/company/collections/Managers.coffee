((app, context) ->

	class context.Manager extends Backbone.Model
		url: ->
			"/company/#{@get('companyId')}/manager"


	class context.Managers extends Backbone.Collection
		model: context.Manager

		url: ->
			"/company/#{@company.id}/managers"

		initialize: (company)->
			@company = company


)(app, app.module('companyListModule'))