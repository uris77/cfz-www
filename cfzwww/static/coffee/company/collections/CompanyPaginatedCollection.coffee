((app, context)->
	
	class context.CompanyPaginatedCollection extends Backbone.Collection
		model: context.Company

		url: ->
			@_nextPage()

		_nextPage: ->
			if @currentPage == 0
				@config.url
			else
				"#{@config.url}/#{@currentPage + 1}/#{@config.pageSize}"

		# Default current page
		currentPage: 0
		config:
			pageSize: 10
			url: '/companies/list'

		getNextPage: =>
			if @currentPage == 0
				@currentPage += 2
			else
				@currentPage += 1
			@fetch()

		parse: (response)->
			meta = new context.ResultsMeta()
			meta.set({total: response.total})
			app.vent.trigger "companies:fetch", response.companies
			app.vent.trigger "companies:fetch:meta", meta
			response.companies

		_collectCompanies: (_company)->
			company = new context.Company(_company)
			company.set({'shareholders': new context.Shareholders(_company.shareholders)})
			company.set({'annualReports': new context.AnnualReports(_company.annual_reports)})
			company

)(app, app.module('companyListModule'))