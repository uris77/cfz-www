((app, context) ->

	class context.Companies extends Backbone.Collection
		model: context.Company
		url: ->
			'/companies/list'

	class context.Shareholders extends Backbone.Collection
		model: context.Shareholder
		url: ->
			'/companies/shareholder/list'

	class context.AnnualReports extends Backbone.Collection
		model: context.AnnualReport
		url: ->
			'/companies/annualreport/list'

)(app, app.module('companyListModule'))