((app, context) ->
	class context.CompanyItemView extends Backbone.Marionette.ItemView 
		template: Handlebars.templates['company_list_body']
		tagName: 'tr'

	class context.CompanyListView extends Backbone.Marionette.CompositeView
		template: Handlebars.templates['company_list_header']
		itemViewContainer: 'tbody'
		itemView: context.CompanyItemView


	# class context.CompanyListView extends Backbone.Marionette.CollectionView
	# 	tagName: 'ul'

)(app, app.module('companyListModule'))