((app, context)->

	class context.ManagerView extends Backbone.Marionette.ItemView
		template: '#manager-tmpl'
		tagName: 'tr'

		events:
			'click #edit-manager-btn': 'editManager'

		editManager: ->
			app.vent.trigger "manager:edit:showForm", @model

	class context.ManagerCollectionView extends Backbone.Marionette.CompositeView
		itemView: context.ManagerView
		itemViewContainer: "tbody"
		template: "#manager-table-tmpl"

		events:
			'click #add-manager-btn': 'addManagerForm'

		addManagerForm: ->
			app.vent.trigger 'company:show:newManagerForm'


	class context.ManagerFormView extends Backbone.Marionette.ItemView
		template: '#manager-form-tmpl'

		events:
			'click #cancel-btn' : 'cancelManagerForm'
			'click #save-btn' : 'saveManager'
			'blur #first-name' : 'requiredFieldAlert'
			'blur #last-name': 'requiredFieldAlert'
			'change #start-date': 'requiredFieldAlert'


		onRender: ->
			_.defer(->
				$('#first-name-alert').hide()
				$('#last-name-alert').hide()
				$('#start-date-alert').hide()
				$('#save-btn').attr('disabled', 'disabled')
				$("#start-date").attr("readonly", "true")

				if _.isUndefined(@model?.id)
					$("#start-date").datepicker
						showWeek: true
						firstDay: 1
						changeMonth: true
						changeYear: true
						showOtherMonths: true
						format: "dd/mm/yyyy"
					.on('changeDate', (ev)->
						$(@).change()
					)

			, [@model])


		initialize: ->
			@validationErrors = ['first-name', 'last-name']


		saveManager: (e)->
			e.preventDefault()
			first_name = $('#first-name').val()
			last_name = $('#last-name').val()
			start_date = $('#start-date').val()
			@model.save({
				first_name: first_name,
				last_name: last_name,
				start_date: start_date},
				{success: (model, response, options)->
					bootbox.alert("Manager saved successfully!", ->
						app.vent.trigger "manager:save:success" 
					)
				})

		cancelManagerForm: (e)->
			e.preventDefault()
			app.vent.trigger 'company:show:cancelManagerForm'

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-btn').attr("disabled", "disabled")
			else
				$('#save-btn').removeAttr("disabled")


	class context.ManagerEditFormView extends Backbone.Marionette.ItemView
		template: '#manager-form-tmpl'

		events:
			'click #cancel-btn' : 'cancelManagerForm'
			'click #save-btn' : 'saveManager'
			'blur #first-name' : 'requiredFieldAlert'
			'blur #last-name': 'requiredFieldAlert'


		onRender: ->
			_.defer(->
				$('#first-name-alert').hide()
				$('#last-name-alert').hide()
				$('#start-date-alert').hide()
				$("#start-date").attr("readonly", "true")

			, [@model])

		initialize: ->
			@validationErrors = []

		saveManager: (e)->
			e.preventDefault()
			first_name = $('#first-name').val()
			last_name = $('#last-name').val()
			start_date = moment(@model.get('start_date')).format('DD/MM/YYYY')
			@model.save({
				first_name: first_name,
				last_name: last_name,
				start_date: start_date},
				{success: (model, response, options)->
					bootbox.alert("Manager saved successfully!", ->
						app.vent.trigger "manager:save:success" 
					)
				})

		cancelManagerForm: (e)->
			e.preventDefault()
			app.vent.trigger 'company:show:cancelManagerForm'

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-btn').attr("disabled", "disabled")
			else
				$('#save-btn').removeAttr("disabled")


)(app, app.module('companyListModule'))
