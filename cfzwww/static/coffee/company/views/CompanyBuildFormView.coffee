((app, context)->
	class context.CompanyFormBuilder extends Backbone.Marionette.ItemView
		template: '#company-tmpl'

		events:
			'click #save-company-btn' : 'saveCompany'
			'blur #first-name' : 'requiredFieldAlert'
			'blur #last-name' : 'requiredFieldAlert'
			'keyup #last-name' : 'requiredFieldAlert'
			'blur #registration-number' : 'requiredFieldAlert'
			'blur #company-name' : 'requiredFieldAlert'
			'change #start-date' : 'requiredFieldAlert'

		firstNameAlert: '#first-name-alert'

		initialize: ->
			app.vent.on "company:save:error", @showErrors
			@validationErrors = ['first-name', 'last-name', 'company-name',
							   'registration-number', 'start-date']

		hideAlertMessages: ->
			$('#first-name-alert').hide()
			$('#last-name-alert').hide()
			$('#start-date-alert').hide()
			$('#incorporation-date-alert').hide()
			$('#registration_number-alert').hide()
			$('#company-name-alert').hide()

		onRender: ->
			_.defer(->
				$("#start-date").datepicker
					showWeek: true
					firstDay: 1
					changeMonth: true
					changeYear: true
					showOtherMonths: true
					format: "dd/mm/yyyy"
					startDate: "01/01/1994"

				$("#incorporation-date").datepicker
					showWeek: true
					firstDay: 1
					changeMonth: true
					changeYear: true
					showOtherMonths: true
					format: "dd/mm/yyyy"
					startDate: moment(new Date()).format("DD/MM/YYYY")
					
				$('#first-name-alert').hide()
				$('#last-name-alert').hide()
				$('#start-date-alert').hide()
				$('#incorporation-date-alert').hide()
				$('#registration-number-alert').hide()
				$('#company-name-alert').hide()
				$('#save-company-btn').attr("disabled", "disabled")
			) 
			@hideAlertMessages()


		saveCompany: (e)->
			e.preventDefault()
			name = $('#company-name').val()
			registration_number = $('#registration-number').val()
			company_type = $('#company-type').val()
			first_name = $('#first-name').val()
			last_name = $('#last-name').val()
			company_status = $('#company-status').val()
			start_date = $('#start-date').val()
			incorporation_date = $('#incorporation-date').val()
			@model.save({
				name: name,
				registration_number: registration_number,
				company_type: company_type,
				first_name: first_name,
				last_name: last_name,
				status: company_status,
				incorporation_date: incorporation_date,
				start_date: start_date},
				{success: (model, response, options)->
					app.vent.trigger "company:save:success", model
				})

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-company-btn').attr("disabled", "disabled")
			else
				$('#save-company-btn').removeAttr("disabled")


	class context.SaveSuccessView extends Backbone.Marionette.ItemView
		template: '#save-success-tmpl'

		onRender: ->
			console.log "company saved: ", @model


)(app, app.module('companyBuildModule'))

