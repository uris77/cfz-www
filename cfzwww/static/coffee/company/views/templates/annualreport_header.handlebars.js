(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['annualreport_header'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"span6\">\n   <div class=\"button-action\">\n   <button class=\"btn btn-small btn-af\" id=\"add-report-btn\">\n      <span>\n         <i class=\"icon-small icon-list-alt\"></i>\n      </span>\n      Add Annual Report\n   </button>\n   </div>\n</div>\n<div id=\"report-form\">\n</div>\n<table class=\"display table table-striped table-bordered\" aria-describedby=\"list-of-reports\">\n   <thead>\n      <tr role=\"row\">\n         <th role=\"columnheader\">Id</th>\n         <th role=\"Date Submitted\">Date Submitted</th>\n         <th role=\"Reporting Year\">Reporting Year</th>\n      </tr>\n   </thead>\n   <tbody></tbody>\n</table>\n\n";
  });
})();