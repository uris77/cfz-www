(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['acceptance_date_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<form class=\"form-horizontal\">\n   <div class=\"control-group\">\n      <label class=\"control-label\" for=\"date_sent_letter_of_acceptance\">\n         Date Letter Sent\n      </label>\n      <div class=\"controls\">\n         <div class=\"input-append\">\n            <input class=\"span10\" type=\"text\" id=\"acceptance-date-letter\" size=\"16\">\n               <span class=\"add-on\"><i class=\"icon-calendar\"></i></span>\n            </input>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <div class=\"controls\">\n            <button class=\"btn btn-primary\" id=\"save-acceptance-date-btn\">\n               <i class=\"icon-ok\"></i>\n               Save\n            </button>\n         </div>\n      </div>\n   </div>\n</form>\n\n";
  });
})();