(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['director_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"well form-horizontal\">\n   <fieldset>\n      <legend>Director</legend>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"first_name\">First Name</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"first_name\" id=\"first-name\"\n               value = ";
  if (stack1 = helpers.first_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.first_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " >\n            </input>\n            <span class=\"alert alert-error\" id=\"first-name-alert\">\n               Please provide a value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"last_name\">Last Name</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"last_name\" id=\"last-name\" \n               value = ";
  if (stack1 = helpers.last_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.last_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " >\n            </input>\n            <span class=\"alert alert-error\" id=\"last-name-alert\">\n               Please provide a value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"start_date\">Start Date</label>\n         <div class=\"controls\">\n            <div class=\"input-append\">\n               <input class=\"span10\" type=\"text\" id=\"start-date\" \n                  name=\"start-date\" value = ";
  if (stack1 = helpers.start_date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.start_date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " >\n                  <span class=\"add-on\"><i class=\"icon-calendar\"></i></span>\n               </input>\n            </div>\n            <span class=\"alert alert-error\" id=\"start-date-alert\">\n               Please provide a value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"end_date\">End Date</label>\n         <div class=\"controls\">\n            <div class=\"input-append\">\n               <input class=\"span10\" type=\"text\" id=\"end-date\" \n                  name=\"end-date\" value = ";
  if (stack1 = helpers.end_date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.end_date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " >\n                  <span class=\"add-on\"><i class=\"icon-calendar\"></i></span>\n               </input>\n            </div>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"director-email\">Email Address</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"director-email\" id=\"email\" \n               value = ";
  if (stack1 = helpers.email) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.email; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " >\n            </input>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"phone1\">Phone 1</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"phone1\" id=\"director-phone1\" \n               value = ";
  if (stack1 = helpers.phone1) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.phone1; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " >\n            </input>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"phone2\">Phone 2</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"phone2\" id=\"director-phone2\" \n               value = ";
  if (stack1 = helpers.phone2) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.phone2; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " >\n            </input>\n         </div>\n      </div>\n\n      <div class=\"control-group\">\n         <div class=\"controls\">\n            <button class=\"btn btn-primary\" id=\"save-btn\">\n               <i class=\"icon-ok\"></i>\n               Save\n            </button>\n            <button class=\"btn btn-danger\" id=\"cancel-btn\">\n               <i class=\"icon-singout\"></i>\n               Cancel\n            </button>\n         </div>\n      </div>\n   </fieldset>\n</form>\n\n";
  return buffer;
  });
})();