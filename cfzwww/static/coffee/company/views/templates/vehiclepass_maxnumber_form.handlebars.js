(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['vehiclepass_maxnumber_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"well form-inline\">\n   <span>\n   <label>\n      Max # of Vehicles\n   </label>\n   <input type=\"text\" id=\"max-vehicles\" value= \"";
  if (stack1 = helpers.max_vehicles) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.max_vehicles; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n   </input>\n   <span id=\"vehiclepass-spinner\">\n   </span>\n   <button type=\"submit\" id=\"save-max-passes-btn\" class=\"btn\">Save</button>\n   </span>\n</form>\n\n";
  return buffer;
  });
})();