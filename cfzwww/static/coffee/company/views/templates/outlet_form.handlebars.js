(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['outlet_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"well form-horizontal\">\n   <fieldset>\n      <legend>Create New Outlet</legend>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"name\">Outlet Name</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"name\" id=\"outlet-name\" value = \"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" required >\n            </input>\n            <span class=\"alert alert-error\" id=\"outlet-name-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"outlet-status\">Outlet Status\n         </label>\n         <div class=\"controls\">\n            <select id=\"outlet-status\">\n               <option value=\"Active\">Active</option>\n               <option value=\"Inactive\">Inactive</option>\n            </select>\n         </div>\n      </div>\n         <div class=\"control-group\">\n            <label class=\"control-label\" for=\"outlet-start-date\">\n               Start Date\n            </label>\n            <div class=\"controls\">\n               <input type=\"text\" id=\"outlet-start-date\" \n                  name=\"outlet-start-date\" value = \"";
  if (stack1 = helpers.start_date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.start_date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n               </input>\n               <span class=\"alert alert-error\" id=\"outlet-start-date-alert\">\n                  Please provide a valid value!\n               </span>\n            </div>\n         </div>\n\n         <fieldset>\n            <legend>Manager</legend>\n            <div class=\"control-group\">\n               <label class=\"control-label\" for=\"outlet-first-name\">First Name</label>\n               <div class=\"controls\">\n                  <input type=\"text\" id=\"outlet-first-name\"\n                     name=\"outlet-first-name\" value = \"";
  if (stack1 = helpers.first_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.first_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" required >\n                  </input>\n                  <span class=\"alert alert-error\" id=\"outlet-first-name-alert\">\n                     Please provide a valid value!\n                  </span>\n               </div>\n            </div>\n            <div class=\"control-group\">\n               <label class=\"control-label\" for=\"outlet-last-name\">Last Name</label>\n               <div class=\"controls\">\n                  <input type=\"text\" id=\"outlet-last-name\"\n                     name=\"outlet-last-name\" value = \"";
  if (stack1 = helpers.last_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.last_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" required>\n                  </input>\n                  <span class=\"alert alert-error\" id=\"outlet-last-name-alert\">\n                     Please provide a valid value!\n                  </span>\n               </div>\n            </div>\n         </fieldset>\n\n         <fieldset>\n            <legend>Contact Numbers</legend>\n            <div class=\"control-group\">\n               <label class=\"control-label\" for=\"outlet-telephone\">Telephone #</label>\n               <div class=\"controls\">\n                  <input type=\"text\" id=\"outlet-telephone\"\n                     name=\"outlet-telephone\" value = \"";
  if (stack1 = helpers.telephone_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.telephone_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" required>\n                  </input>\n                  <span class=\"alert alert-error\" id=\"outlet-telephone-alert\">\n                     Please provide a valid value!\n                  </span>\n               </div>\n            </div>\n            <div class=\"control-group\">\n               <label class=\"control-label\" for=\"outlet-fax\">Fax #</label>\n               <div class=\"controls\">\n                  <input type=\"text\" id=\"outlet-fax\"\n                     name=\"outlet-fax\" value = \"";
  if (stack1 = helpers.fax_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.fax_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n                  </input>\n               </div>\n            </div>\n         </fieldset>\n\n         <fieldset>\n            <legend>Address</legend>\n            <div class=\"control-group\">\n               <label class=\"control-label\" for=\"outlet-plaza\">Plaza</label>\n               <div class=\"controls\">\n                  <input type=\"text\" id=\"outlet-plaza\"\n                     name=\"outlet-plaza\" value = \"";
  if (stack1 = helpers.line1) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.line1; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" required>\n                  </input>\n                  <span class=\"alert alert-error\" id=\"outlet-plaza-alert\">\n                     Please provide a valid value!\n                  </span>\n               </div>\n            </div>\n            <div class=\"control-group\">\n               <label class=\"control-label\" for=\"outlet-locale\">Locale</label>\n               <div class=\"controls\">\n                  <input type=\"text\" id=\"outlet-locale\"\n                     name=\"outlet-locale\" value = \"";
  if (stack1 = helpers.line2) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.line2; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" required>\n                  </input>\n                  <span class=\"alert alert-error\" id=\"outlet-locale-alert\">\n                     Please provide a valid value!\n                  </span>\n               </div>\n            </div>\n         </fieldset>\n\n         <div class=\"control-group\">\n            <div class=\"controls\">\n               <button class=\"btn btn-primary\" id=\"save-outlet-btn\">\n                  <i class=\"icon-ok\"></i>\n                  Save\n               </button>\n               <button class=\"btn btn-danger\" id=\"cancel-outlet-btn\">\n                  <i class=\"icon-singout\"></i>\n                  Cancel\n               </button>\n            </div>\n         </div>\n\n   </fieldset>\n</form>\n";
  return buffer;
  });
})();