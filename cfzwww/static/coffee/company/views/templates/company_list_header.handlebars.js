(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['company_list_header'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<table class=\"display table table-striped table-bordered\">\n   <thead>\n      <tr role=\"row\">\n         <th role=\"Name\">Company Name</th>\n         <th role=\"Registraion Number\">Registration Number</th>\n         <th role=\"Telephone Number\">Telephone #</th>\n         <th role=\"Date Created\">Date Created</th>\n         <th/>\n      </tr>\n   </thead>\n   <tbody></tbody>\n</table>\n";
  });
})();