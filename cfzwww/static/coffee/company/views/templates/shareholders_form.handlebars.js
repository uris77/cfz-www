(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['shareholders_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"well form-horizontal\">\n   <fieldset>\n      <legend>Shareholder</legend>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"first_name\">First Name</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"first_name\" id=\"shareholder-first-name\"\n               value = \"";
  if (stack1 = helpers.first_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.first_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" \n               id=\"shareholder-first-name-alert\">\n               Please provide a value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"last_name\">Last Name</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"last_name\" \n               id=\"shareholder-last-name\" value = \"";
  if (stack1 = helpers.last_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.last_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" \n               id=\"shareholder-last-name-alert\">\n               Please provide a value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"shares\">Shares</label>\n         <div class=\"controls\">\n            <input type=\"number\" id=\"shares\"  pattern=\"\\d*\"\n               name=\"shares\" value = \"";
  if (stack1 = helpers.shares) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.shares; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" />\n            <span class=\"alert alert-error\" id=\"shares-alert\">\n               Please provide a value!\n            </span>\n         </div>\n      </div>\n\n      <div class=\"control-group\">\n         <div class=\"controls\">\n            <button class=\"btn btn-primary\" id=\"save-shareholder-btn\">\n               <i class=\"icon-ok\"></i>\n               Save\n            </button>\n            <button class=\"btn btn-danger\" id=\"cancel-shareholder-btn\">\n               <i class=\"icon-singout\"></i>\n               Cancel\n            </button>\n         </div>\n      </div>\n   </fieldset>\n</form>\n\n";
  return buffer;
  });
})();