(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['address_table'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div id=\"alert-space\">\n</div>\n\n<table class=\"display table table-striped table-bordered\">\n   <thead>\n      <tr role=\"row\">\n         <th role=\"Plaza\">Plaza</th>\n         <th role=\"Locale\">Locale</th>\n         <th/>\n      </tr>\n   </thead>\n   <tbody>\n      <td>";
  if (stack1 = helpers.line1) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.line1; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n      <td>";
  if (stack1 = helpers.line2) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.line2; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n      <td>\n         <button class=\"btn btn-primary\" id=\"edit-btn\" title=\"Remove Authorized Representative\">\n            Edit\n         </button>\n      </td>\n   </tbody>\n</table>\n\n";
  return buffer;
  });
})();