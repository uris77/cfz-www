(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['company_nav'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "   <li id=\"generalInfo-tab-item\">\n      <a id=\"generalInfo-tab\" href=\"#\">General Info</a>\n   </li>\n   <li id=\"contactNumbers-tab-item\">\n      <a id=\"contactNumbers-tab\" href=\"#\">Contact Info</a>\n   </li>\n   <li id=\"address-tab-item\">\n      <a id=\"address-tab\" href=\"#\">Address</a>\n   </li>\n   <li id=\"director-tab-item\">\n      <a id=\"director-tab\" href=\"#\">Directors</a>\n   </li>\n   <li id=\"shareholders-tab-item\">\n      <a id=\"shareholders-tab\" href=\"#\">Shareholders</a>\n   </l>\n   <li id=\"goods-categories-tab-item\">\n      <a id=\"goods-categories-tab\" href=\"#\">Goods</a>\n   </l>\n   <li id=\"annualreports-tab-item\">\n      <a id=\"annualreports-tab\" href=\"#\">Annual Reports</a>\n   </li>\n   <li id=\"courtesy-pass-tab-item\">\n      <a id=\"courtesy-pass-tab\" href=\"#\">Courtesy Stickers</a>\n   </li>\n   <li id=\"stickers-tab-item\">\n      <a id=\"stickers-tab\" href=\"#\">Vehicle Stickers</a>\n   </li>\n   <li id=\"outlets-tab-item\">\n      <a id=\"outlets-tab\" href=\"#\">Outlets</a>\n   </li>\n\n";
  });
})();