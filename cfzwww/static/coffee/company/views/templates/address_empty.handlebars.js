(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['address_empty'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"well form-horizontal\">\n   <fieldset>\n      <legend>Address</legend>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"plaza\">Plaza</label>\n         <div class=\"controls\">\n            <input type=\"text\" id=\"plaza\" name=\"plaza\" required value = \"";
  if (stack1 = helpers.line1) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.line1; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ></input>\n         </div>   `\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"locale\">\n            Locale\n         </label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"locale\" id=\"locale\" required value = \"";
  if (stack1 = helpers.line2) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.line2; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ></input>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <div class=\"controls\">\n            <button class=\"btn btn-primary\" id=\"save-address-btn\">\n               <i class=\"icon-ok\"></i>\n               Save\n            </button>\n             <button class=\"btn btn-danger\" id=\"address-return-btn\">\n               <i class=\"icon-signout\"></i>\n               Cancel\n            </button>\n         </div>\n      </div>\n   </fieldset>\n</form>\n\n";
  return buffer;
  });
})();