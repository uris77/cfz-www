(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['annualreport_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"well form-horizontal\">\n   <fieldset>\n      <legend>Annual Report</legend>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"submitted_date\">Date Submitted</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"submitted_date\" id=\"report-submitted-date\"\n               value = \"";
  if (stack1 = helpers.submitted_date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.submitted_date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" \n               id=\"report-submitted-date-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"reporting-year\">Reporting Year</label>\n         <div class=\"controls\">\n            <input type=\"number\" id=\"reporting-year\" type=\"number\" \n               name=\"reporting-year\" value = \"";
  if (stack1 = helpers.reporting_year) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.reporting_year; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" required >\n            </input>\n            <span class=\"alert alert-error\" id=\"reporting-year-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n\n      <div class=\"control-group\">\n         <div class=\"controls\">\n            <button class=\"btn btn-primary\" id=\"save-report-btn\">\n               <i class=\"icon-ok\"></i>\n               Save\n            </button>\n            <button class=\"btn btn-danger\" id=\"cancel-report-btn\">\n               <i class=\"icon-singout\"></i>\n               Cancel\n            </button>\n         </div>\n      </div>\n   </fieldset>\n</form>\n\n";
  return buffer;
  });
})();