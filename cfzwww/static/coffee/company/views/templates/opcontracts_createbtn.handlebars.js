(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['opcontracts_createbtn'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<button class=\"btn btn-small btn-af\" id=\"add-contract-btn\">\n   <span>\n      <i class=\"icon-small icon-list-alt\"></i>\n   </span>\n   Add Operations Contract\n</button>\n\n\n";
  });
})();