(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['outlet_create_button'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"span6\">\n   <div class=\"button-action\">\n      <button  class=\"btn btn-small btn-af\" \n         id=\"add-outlet-btn\">\n         <span>\n            <i class=\"icon-small icon-list-alt\"></i>\n         </span>\n         Add Outlet\n      </button>\n   </div>\n</div>\n\n";
  });
})();