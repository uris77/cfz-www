(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['vehiclepass_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"well form-horizontal\">\n   <fieldset>\n      <legend>Vehicle Sticker</legend>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"owner\">Owner</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"owner\" id=\"vehicle-owner\"\n               value = \"";
  if (stack1 = helpers.owner) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.owner; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" \n               id=\"vehicle-owner-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"vehicle-year\">Vehicle Year</label>\n         <div class=\"controls\">\n            <input type=\"number\" name=\"vehicle-year\" \n               id=\"vehicle-year\" value = \"";
  if (stack1 = helpers.vehicle_year) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.vehicle_year; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" \n               id=\"vehicle-year-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"vehicle-model\">Vehicle Model</label>\n         <div class=\"controls\">\n            <input type=\"text\" id=\"vehicle-model\" \n               name=\"vehicle-model\" value = \"";
  if (stack1 = helpers.vehicle_model) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.vehicle_model; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" id=\"vehicle-model-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"color\">Color</label>\n         <div class=\"controls\">\n            <input type=\"text\" id=\"vehicle-color\" \n               name=\"vehicle-color\" value = \"";
  if (stack1 = helpers.color) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.color; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" id=\"vehicle-color-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"license-plate\">\n            License Plate Number\n         </label>\n         <div class=\"controls\">\n            <input type=\"text\" id=\"license-plate\" \n               name=\"license-plate\" value = \"";
  if (stack1 = helpers.license_plate) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.license_plate; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" id=\"license-plate-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"sticker-number\">\n            Sticker Number\n         </label>\n         <div class=\"controls\">\n            <input type=\"text\" id=\"sticker-number\" \n               name=\"sticker-number\" value = \"";
  if (stack1 = helpers.sticker_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.sticker_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" id=\"sticker-number-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"date-issued\">\n            Date Issued\n         </label>\n         <div class=\"controls\">\n            <input type=\"text\" id=\"date-issued\" \n               name=\"date-issued\" value = \"";
  if (stack1 = helpers.date_issued) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.date_issued; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" id=\"date-issued-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"month-expired\">\n            Month Expired\n         </label>\n         <div class=\"controls\">\n            <select name=\"month-expired\" id=\"month-expired\">\n               <option value=\"January\">January</option>\n               <option value=\"February\">February</option>\n               <option value=\"March\">March</option>\n               <option value=\"April\">April</option>\n               <option value=\"May\">May</option>\n               <option value=\"June\">June</option>\n               <option value=\"July\">July</option>\n               <option value=\"August\">August</option>\n               <option value=\"September\">September</option>\n               <option value=\"October\">October</option>\n               <option value=\"November\">November</option>\n               <option value=\"December\">December</option>\n            </select>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"receipt-number\">\n            Receipt Number\n         </label>\n         <div class=\"controls\">\n            <input type=\"text\" id=\"receipt-number\" \n               name=\"receipt-number\" value = \"";
  if (stack1 = helpers.receipt_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.receipt_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" id=\"receipt-number-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\" id=\"issued-by\">\n         <label class=\"control-label\" for=\"issued-by\">\n            Issued By\n         </label>\n         <div class=\"controls\">\n            <input type=\"text\" disabled\n               name=\"issued-by\" value = \"";
  if (stack1 = helpers.issued_by) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.issued_by; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n         </div>\n      </div>\n\n      <div class=\"control-group\">\n         <div class=\"controls\">\n            <button class=\"btn btn-primary\" id=\"save-sticker-btn\">\n               <i class=\"icon-ok\"></i>\n               Save\n            </button>\n            <button class=\"btn btn-danger\" id=\"cancel-sticker-btn\">\n               <i class=\"icon-singout\"></i>\n               Cancel\n            </button>\n         </div>\n      </div>\n   </fieldset>\n</form>\n\n";
  return buffer;
  });
})();