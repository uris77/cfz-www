((app, context)->

	class context.VehicleStickerView extends Backbone.Marionette.ItemView
		template: '#stickers-tmpl'	
		tagName: 'tr'


	class context.VehicleStickersCollectionView extends Backbone.Marionette.CompositeView
		template: '#stickers-table-tmpl'
		itemViewContainer: 'tbody'
		itemView: context.VehicleStickerView

		events:
			'click #add-sticker-btn': 'showForm'

		showForm: ->
			app.vent.trigger 'stickers:new:form'


	class context.VehicleStickerFormView extends Backbone.Marionette.ItemView
		template: '#stickers-form-tmpl'

		events: 
			'click #cancel-sticker-btn': 'cancelForm'
			'click #save-sticker-btn': 'saveSticker'
			'keyup #vehicle-owner': 'requiredFieldAlert'
			'keyup #vehicle-model': 'requiredFieldAlert'
			'keyup #vehicle-year': 'requiredFieldAlert'
			'keyup #vehicle-color': 'requiredFieldAlert'
			'keyup #license-plate': 'requiredFieldAlert'
			'keyup #sticker-number': 'requiredFieldAlert'
			'keyup #receipt-number': 'requiredFieldAlert'
			'keyup #issued-by': 'requiredFieldAlert'

		onRender: ->
			_.defer(->
				$('#vehicle-owner-alert').hide()
				$('#vehicle-year-alert').hide()
				$('#vehicle-model-alert').hide()
				$('#vehicle-color-alert').hide()
				$('#sticker-number-alert').hide()
				$('#date-issued-alert').hide()
				$('#license-plate-alert').hide()
				$('#receipt-number-alert').hide()
				$('#issued-by-alert').hide()
				$('#save-sticker-btn').attr("disabled", "disabled")

				$("#date-issued").datepicker
					showWeek: true
					firstDay: 1
					changeMonth: true
					changeYear: true
					showOtherMonths: true
					format: "dd/mm/yyyy"
				.on('changeDate', (ev)->
					$(@).change()
				)
			)

		initialize: ->
			@validationErrors = []

		cancelForm: (e)->
			e.preventDefault()
			app.vent.trigger "stickers:cancelForm"

		saveSticker: (e)->
			e.preventDefault()
			owner = $('#vehicle-owner').val()
			vehicle_year = $('#vehicle-year').val()
			vehicle_model = $('#vehicle-model').val()
			vehicle_color = $('#vehicle-color').val()
			license_plate = $('#license-plate').val()
			color = $('#vehicle-color').val()
			sticker_number = $('#sticker-number').val()
			date_issued = $('#date-issued').val()
			receipt_number = $('#receipt-number').val()
			issued_by = $('#issued-by').val()
			month_expired = $('#month-expired').val()
			@model.save({
				owner: owner,
				vehicle_year: vehicle_year,
				vehicle_model: vehicle_model,
				color: color,
				license_plate: license_plate,
				sticker_number: sticker_number,
				date_issued: date_issued,
				receipt_number: receipt_number,
				issued_by: issued_by,
				month_expired: month_expired},
				{success: (model, response, options)->
					bootbox.alert('Vehicle Sticker Saved!', ->
						app.vent.trigger 'stickers:save:success'
					)
				})

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-sticker-btn').attr("disabled", "disabled")
			else
				$('#save-sticker-btn').removeAttr("disabled")


)(app, app.module('companyListModule'))
