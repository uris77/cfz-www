((app, context)->

	class context.AnnualReportView extends Backbone.Marionette.ItemView
		template: '#annualreports-tmpl'
		tagName: 'tr'


	class context.AnnualReportsCollectionView extends Backbone.Marionette.CompositeView
		itemView: context.AnnualReportView
		itemViewContainer: 'tbody'
		template: '#annualreports-table-tmpl'

		events: 
			'click #add-report-btn': 'showForm'

		showForm: ->
			app.vent.trigger 'annualreport:new:form'


	class context.AnnualReportFormView extends Backbone.Marionette.ItemView
		template: '#annualreport-form-tmpl'

		events:
			'click #cancel-report-btn': 'cancelForm'
			'keyup #report-submitted-date': 'requiredFieldAlert'
			'keyup #report-start-date': 'requiredFieldAlert'
			'keyup #report-end-date': 'requiredFieldAlert'
			'click #save-report-btn': 'saveAnnualReport'

		onRender: ->
			_.defer(->
				$('#report-submitted-date-alert').hide()
				$('#report-start-date-alert').hide()
				$('#report-end-date-alert').hide()
				$('#report-submitted-date').attr("readonly", "true")
				$('#report-start-date').attr("readonly", "true")
				$('#report-end-date').attr("readonly", "true")

				$('#report-submitted-date').datepicker
						showWeek: true
						firstDay: 1
						changeMonth: true
						changeYear: true
						showOtherMonths: true
						format: "dd/mm/yyyy"
					.on('changeDate', (ev)->
						$(@).keyup()
					)

				$('#report-start-date').datepicker
						showWeek: true
						firstDay: 1
						changeMonth: true
						changeYear: true
						showOtherMonths: true
						format: "dd/mm/yyyy"
					.on('changeDate', (ev)->
						$(@).keyup()
					)

				$('#report-end-date').datepicker
						showWeek: true
						firstDay: 1
						changeMonth: true
						changeYear: true
						showOtherMonths: true
						format: "dd/mm/yyyy"
					.on('changeDate', (ev)->
						$(@).keyup()
					)
			)

		initialize: ->
			#@validationErrors = ['report-submitted-date','report-start-date', 'report-end-date']
			@validationErrors = []

		cancelForm: (e)->
			e.preventDefault()
			app.vent.trigger 'annualreport:cancelForm'

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-report-btn').attr("disabled", "disabled")
			else
				$('#save-report-btn').removeAttr("disabled")

		saveAnnualReport: (e)->
			e.preventDefault()
			submitted_date = $('#report-submitted-date').val()
			start_date = $('#report-start-date').val()
			end_date = $('#report-end-date').val()
			@model.save({
				date_submitted: submitted_date,
				start_date: start_date,
				end_date: end_date},
				{success: (model, response, options)->
					bootbox.alert('Annual Report Saved!', ->
						app.vent.trigger "annualreport:save:success"
					)

				})

)(app, app.module('companyListModule'))
