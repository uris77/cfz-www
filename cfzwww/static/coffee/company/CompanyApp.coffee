window.app = 
	module: (->
		modules = {}

		(name) ->
			if not modules[name]
				modules[name] = Views: {}
			modules[name]
	)()

	vent:
		_.extend {}, Backbone.Events

$ ->

	app.CompanyApp = new Backbone.Marionette.Application()
	app.CompanyApp.addRegions
		mainRegion: '#tab-content',
		navBarRegion: '#company-navbar'
		managersRegion: '#tab-managers'
		managersItemRegion: '#managers-tbody'

	companyModule = app.module('companyListModule')

	companyModule.company = new companyModule.Company()

	class companyModule.CompanyAppController extends Backbone.Marionette.Controller
		initialize: ->
			app.vent.on "company:show:generalInformation", @renderGeneralInformation, @
			app.vent.on "company:show:contactNumbers", @showContactNumbers
			app.vent.on "company:show:address", @showAddress
			app.vent.on "company:show:manager", @showManagers, @
			app.vent.on "company:show:shareholders", @showShareholders, @
			app.vent.on "company:show:annualreports", @showAnnualReports, @
			app.vent.on "company:show:newManagerForm", @showNewManagerForm, @
			app.vent.on "company:show:cancelManagerForm", @cancelManagerForm, @
			app.vent.on "company:show:stickers", @showVehicleStickers
			app.vent.on "company:show:outlets", @showOutlets
			app.vent.on "manager:save:success", @showManagers, @
			app.vent.on "manager:edit:showForm", @showManagerEditForm, @
			app.vent.on "shareholder:edit:showForm", @showShareholderEditForm, @
			app.vent.on "shareholder:new:form", @showShareholderForm, @
			app.vent.on "shareholder:cancelForm", @cancelShareholderForm, @
			app.vent.on "shareholder:save:success", @showShareholders, @
			app.vent.on "annualreport:new:form", @showAnnualReportForm, @
			app.vent.on "annualreport:cancelForm", @cancelAnnualReportForm, @
			app.vent.on "annualreport:save:success", @showAnnualReports, @
			app.vent.on "stickers:new:form", @showVehicleStickerForm, @
			app.vent.on "stickers:cancelForm", @cancelVehicleStickerForm, @
			app.vent.on "stickers:save:success", @showVehicleStickers, @

		renderGeneralInformation: =>
			app.generalInformationView = new companyModule.GeneralInformationView({model: companyModule.company})
			app.CompanyApp.mainRegion.show(app.generalInformationView)

		showContactNumbers: ->
			app.contactNumbers = new companyModule.ContactNumbers()
			app.contactNumbers.set('id', companyModule.company.get('id'))
			app.contactNumbers.fetch({
				success: (model)->
					app.contactNumbersView = new companyModule.ContactNumbersView({model: model})
					app.CompanyApp.mainRegion.show(app.contactNumbersView)
			})

		showAddress: ->
			app.addressModel = new companyModule.Address()
			app.addressModel.set('id', companyModule.company.get('id'))
			app.addressModel.fetch({
				success: (model)->
					app.addressView = new companyModule.AddressView({model: model})
					app.CompanyApp.mainRegion.show(app.addressView)
				})

		showManagers: ->
			app.managers = new companyModule.Managers(companyModule.company)
			app.managers.fetch({
				success: (model)->
					app.managerView = new companyModule.ManagerCollectionView
						collection: model
						itemView: companyModule.ManagerView
					app.CompanyApp.mainRegion.show(app.managerView)
				})

		showNewManagerForm: ->
			manager = new companyModule.Manager
				companyId: companyModule.company.get('id')
				start_date: moment(new Date()).format("DD/MM/YYYY")
				first_name: ''
				last_name: ''
			formView = new companyModule.ManagerFormView(model: manager)
			app.CompanyApp.mainRegion.show(formView)

		showManagerEditForm: (manager)->
			manager.set('companyId', companyModule.company.get('id'))
			formView = new companyModule.ManagerEditFormView(model: manager)
			app.CompanyApp.mainRegion.show(formView)


		cancelManagerForm: ->
			@showManagers(companyModule.company)

		showShareholders: ->
			app.shareholders = new companyModule.Shareholders(companyModule.company)
			app.shareholders.fetch({
				success: (model)->
					app.shareholderView = new companyModule.ShareholderCollectionView
						collection: model
						itemView: companyModule.ShareholderView
					app.CompanyApp.mainRegion.show(app.shareholderView)
				})

		showShareholderEditForm: (shareholder) ->
			shareholder.set('companyId', companyModule.company.get('id'))
			formView = new companyModule.ShareholderFormView(model: shareholder)
			app.CompanyApp.mainRegion.show(formView)

		showShareholderForm: ->
			shareholder = new companyModule.Shareholder
				companyId: companyModule.company.get('id')
				first_name: ''
				last_name: ''
				shares: 0
			formView = new companyModule.ShareholderFormView(model: shareholder)
			app.CompanyApp.mainRegion.show(formView)

		cancelShareholderForm: ->
			@showShareholders()

		# ANNUAL REPORTS
		showAnnualReports: ->
			app.annualReports = new companyModule.AnnualReports(companyModule.company)
			app.annualReports.fetch({
				success: (model)->
					app.annualReportsView = new companyModule.AnnualReportsCollectionView
						collection: model
						itemView: companyModule.AnnualReportView
					app.CompanyApp.mainRegion.show(app.annualReportsView)
				})

		showAnnualReportForm: ->
			annualReport = new companyModule.AnnualReport
				companyId: companyModule.company.get('id')
				submitted_date: moment(new Date()).format("DD/MM/YYYY")
				start_date: moment(new Date()).format("DD/MM/YYYY")
				end_date: moment(new Date()).format("DD/MM/YYYY")
			formView = new companyModule.AnnualReportFormView(model: annualReport)
			app.CompanyApp.mainRegion.show(formView)

		cancelAnnualReportForm: ->
			@showAnnualReports()

		showVehicleStickers: ->
			app.stickers = new companyModule.VehicleStickers(companyModule.company)
			app.stickers.fetch({
				success: (model)->
					app.stickersView = new companyModule.VehicleStickersCollectionView
						collection: model
						itemView: companyModule.VehicleStickerView
					app.CompanyApp.mainRegion.show(app.stickersView)
				})

		showVehicleStickerForm: ->
			sticker = new companyModule.VehicleSticker
				companyId: companyModule.company.get('id')
				date_issued: moment(new Date()).format('DD/MM/YYYY')
			formView = new companyModule.VehicleStickerFormView(model: sticker)
			app.CompanyApp.mainRegion.show(formView)

		cancelVehicleStickerForm: ->
			@showVehicleStickers()

		showOutlets: ->
			app.outletLayout = new companyModule.OutletLayout()
			console.log "outletLayout: ", app.outletLayout
			app.outletBtns = new companyModule.CreateOutletView
				model: companyModule.company
			console.log "app.outletBtns: ", app.outletBtns
			app.outletLayout.render()
			app.outlets = new companyModule.Outlets(companyModule.company)
			app.outlets.fetch({
				success: (model)->
					app.outletsView = new companyModule.OutletsCollectionView
						collection: model
						itemView: companyModule.outletsView
						company: companyModule.company
					app.outletLayout.listRegion.show(app.outletsView)

			})
			app.CompanyApp.mainRegion.show(app.outletLayout)
			app.outletLayout.btnRegion.show(app.outletBtns)


	app.CompanyApp.addInitializer( ->
		app.CompanyApp.navBarRegion.show(new companyModule.CompanyInformationNav())
		locationpath = location.pathname.split('/')
		companyId = locationpath[locationpath.length - 1]
		companyModule.company.set("id", companyId)
		app.controller = new companyModule.CompanyAppController()
		new companyModule.CompanyAppRouter({controller: app.controller})
		companyModule.company.fetch()
		_.delay(
			app.controller.renderGeneralInformation, 1000
		)
		Backbone.history.start()
	)

	app.CompanyApp.start()

