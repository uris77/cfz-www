window.app = 
	module: (->
		modules = {}

		(name) ->
			if not modules[name]
				modules[name] = Views: {}
			modules[name]
	)()

	vent:
		_.extend {}, Backbone.Events

$ ->
	app.CompanyBuilderApp = new Backbone.Marionette.Application()
	app.CompanyBuilderApp.addRegions
		mainRegion: '#content'

	companyBuildModule = app.module('companyBuildModule')

	class app.CompanyBuilderController extends Backbone.Marionette.Controller
		initialize: ->
			app.vent.on "company:save:success", @successfulSave
			@showForm()


		showForm: ->
			company = new companyBuildModule.CompanyFormModel()
			company.set('start_date', moment(new Date()).format('DD/MM/YYYY'))
			formView = new companyBuildModule.CompanyFormBuilder(model:company)
			app.CompanyBuilderApp.mainRegion.show(formView)

		successfulSave: (company)->
			app.CompanyBuilderApp.mainRegion.show(new companyBuildModule.SaveSuccessView({model: company}))

	app.CompanyBuilderApp.addInitializer( ->
		app.controller = new app.CompanyBuilderController()
		Backbone.history.start()
	)

	app.CompanyBuilderApp.start()
