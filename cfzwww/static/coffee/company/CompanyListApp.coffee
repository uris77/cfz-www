window.app = 
	module: (->
		modules = {}

		(name) ->
			if not modules[name]
				modules[name] = Views: {}
			modules[name]
	)()

	vent:
		_.extend {}, Backbone.Events

$ ->

	app.CompanyListApp = new Backbone.Marionette.Application()
				
	app.CompanyListApp.addRegions
		mainRegion: '#pagination',
		resultsMetaRegion: '#resultsMeta'

	companyListModule = app.module("companyListModule")
	app.paginatedCompanyCollection = new companyListModule.CompanyPaginatedCollection()

	app.CompanyListApp.addInitializer( ->
		new companyListModule.CompanyListRouter()
		paginationView = new companyListModule.CompanyListView
			collection: app.paginatedCompanyCollection,
			
		#itemView: companyListModule.CompanyItemView

		app.CompanyListApp.mainRegion.show(paginationView)

		Backbone.history.start()
	)

	class app.ResultsMetaView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['results_count']
		className: "alert alert-message"

	class CompanyController extends Backbone.Marionette.Controller
		initialize: ->
			@resultsMetaView = new app.ResultsMetaView()

		showResultsMeta: (metaInformation)=>
			@resultsMetaView.model = metaInformation
			app.CompanyListApp.resultsMetaRegion.show(@resultsMetaView)

	app.controller = new CompanyController()
	app.vent.on "companies:fetch:meta", app.controller.showResultsMeta

	app.CompanyListApp.start()
	app.paginatedCompanyCollection.fetch()
