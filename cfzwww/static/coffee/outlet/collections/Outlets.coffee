((app, context) ->

	class context.Outlet extends Backbone.Model
		url: ->
			"/outlet"

	class context.Outlets extends Backbone.Collection
		url: ->
			"/company/#{@company.id}/outlets"

		initialize: (company)->
			@company = company


)(app, app.module('companyListModule'))
