((app, context)->
	class context.CreateOutletView extends Backbone.Marionette.ItemView
		template: '#create-tmpl'

		events:
			'click #save-outlet-btn' : 'saveCompany'
			'blur #outlet-name' : 'requiredFieldAlert'
			'change #start-date' : 'requiredFieldAlert'

		initialize: ->
			app.vent.on "company:save:error", @showErrors
			@validationErrors = ['outlet-name', 'start-date']

		hideAlertMessages: ->
			$('#outlet-name-alert').hide()
			$('#start-date-alert').hide()

		onRender: ->
			_.defer(->
				$("#start-date").datepicker
					showWeek: true
					firstDay: 1
					changeMonth: true
					changeYear: true
					dateFormat: "dd/mm/yy"
					
				$('#outlet-name-alert').hide()
				$('#start-date-alert').hide()
				$('#save-outlet-btn').attr("disabled", "disabled")
			) 


		saveCompany: (e)->
			e.preventDefault()
			name = $('#outlet-name').val()
			start_date = $('#start-date').val()
			console.log "saved..."
			# @model.save({
			# 	name: name,
			# 	start_date: start_date},
			# 	{success: (model, response, options)->
			# 		app.vent.trigger "company:save:success", model
			# 	})

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-outlet-btn').attr("disabled", "disabled")
			else
				$('#save-outlet-btn').removeAttr("disabled")


	class context.SaveSuccessView extends Backbone.Marionette.ItemView
		template: '#save-success-tmpl'

		onRender: ->
			console.log "Outlet saved: ", @model


)(app, app.module('outletModule'))

