window.app = 
	module: (->
		modules = {}

		(name) ->
			if not modules[name]
				modules[name] = Views: {}
			modules[name]
	)()

	vent:
		_.extend {}, Backbone.Events

$ ->

app.CreateOutletApp = new Backbone.Marionette.Application()
app.CreateOutletApp.addRegions
	mainRegion: '#content'

outletModule = app.module('outletModule')

class app.CreateOutletAppController extends Backbone.Marionette.Controller
	initialize: ->
		app.vent.on "outlet:save:success", @successfulSave
		@showForm()

	showForm: ->
		outlet = new outletModule.OutletFormModel()
		formView = new outletModule.CreateOutletView(model: outlet)
		app.CreateOutletApp.mainRegion.show(formView)

	successfulSave: (outlet)->
		window.navigate("/outlet/#{outlet.id}")

app.CreateOutletApp.addInitializer( ->
	app.controller = new app.CreateOutletAppController()
	Backbone.history.start()
)

app.CreateOutletApp.start()