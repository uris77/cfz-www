window.app = 
	module: (->
		modules = {}

		(name) ->
			if not modules[name]
				modules[name] = Module: {}
			modules[name]
	)()

	vent:
		_.extend {}, Backbone.Events

$ ->

	CompanyModule = app.module('companyListModule')

	describe "CompanyPaginatedCollection", ->
		beforeEach ->
			@companyPaginatedCollection = new CompanyModule.CompanyPaginatedCollection

		afterEach ->
			@companyPaginatedCollection = null

		describe "Url", ->
			it "should be /companies/list if pageNumber is 0", ->
				expect(@companyPaginatedCollection.url()).toEqual('/companies/list')

			it "should be that of the next page", ->
				@companyPaginatedCollection.currentPage = 1
				expect(@companyPaginatedCollection.url()).toEqual('/companies/list/2/10')

		describe "Current Page when paginating", ->
			it "should be increased by two if we are fetching page 2", ->
				currentPage = @companyPaginatedCollection.currentPage
				@companyPaginatedCollection.getNextPage()
				expect(@companyPaginatedCollection.currentPage).toEqual(currentPage + 2)

			it "should be increased by one if we are fetching page greater than 2", ->
				@companyPaginatedCollection.getNextPage()
				currentPage = @companyPaginatedCollection.currentPage
				@companyPaginatedCollection.getNextPage()
				expect(@companyPaginatedCollection.currentPage).toEqual(currentPage + 1)

