import cryptacular.bcrypt

from sqlalchemy import (
    Column,
    Text)

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    synonym,
    sessionmaker)

from sqlalchemy.types import (
    Integer,
    Unicode)

from zope.sqlalchemy import ZopeTransactionExtension

from pyramid.security import (
    Everyone,
    Authenticated,
    ALL_PERMISSIONS,
    Allow)

from cfzpersistence.repository.company import CompanyRepository
from cfzpersistence.repository.categories import GoodsCategoryRepository
from cfzpersistence.repository.passes import CourtesyPassRepository
from cfzpersistence.repository.authorized_representative import (
    AuthorizedRepresentativeRepository,
    RepresentationTypeRepository)
from cfzpersistence.repository.auth import (UserAccountRepository,
                                            UserGroupRepository)
from cfzpersistence.repository.company_type import CompanyTypeRepository
from cfzpersistence.repository.operational_contract import (
    OperationalContractRepository)
from cfzpersistence.repository.director import DirectorRepository
from cfzpersistence.repository.letters import AccessLetterRepository

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

crypt = cryptacular.bcrypt.BCRYPTPasswordManager()

company_repository = CompanyRepository(DBSession)
goodscategory_repository = GoodsCategoryRepository(DBSession)
courtesypass_repository = CourtesyPassRepository(DBSession)
representation_repository = RepresentationTypeRepository(DBSession)
authorizedrep_repository = AuthorizedRepresentativeRepository(DBSession)
user_repository = UserAccountRepository(DBSession)
group_repository = UserGroupRepository(DBSession)
company_type_repository = CompanyTypeRepository(DBSession)
opcontract_repository = OperationalContractRepository(DBSession)
director_repository = DirectorRepository(DBSession)
access_letter_repository = AccessLetterRepository(DBSession)

cfz_ceo = u"Raul Rosado"
cfz_office = u"#316 Freedom Avenue"
processing_fee = "6.00"


def hash_password(password):
    return unicode(crypt.encode(password))


class User(Base):
    """Appplication's User Model"""
    __tablename__ = "users"
    user_id = Column(Integer, primary_key=True)
    username = Column(Unicode(20), unique=True)
    name = Column(Unicode(50))
    email = Column(Unicode(50))

    _password = Column('password', Unicode(60))

    def _get_password(self):
        return self.password

    def _set_password(self, password):
        self._password = hash_password(password)

    password = property(_get_password, _set_password)
    password = synonym('_password', descriptor=password)

    def __init__(self, username, password, name, email):
        self.username = username
        self.name = name
        self.email = email
        self.password = password

    @classmethod
    def get_by_username(cls, username):
        return DBSession.query(cls).filter(cls.username == username).first()

    @classmethod
    def check_password(cls, username, password):
        user = cls.get_by_username(username)
        if not user:
            return False
        return crypt.check(user.password, password)


class MyModel(Base):
    __tablename__ = 'models'
    id = Column(Integer, primary_key=True)
    name = Column(Text, unique=True)
    value = Column(Integer)

    def __init__(self, name, value):
        self.name = name
        self.value = value


class RootFactory(object):
    __acl__ = [
        (Allow, Authenticated, 'list'),
        (Allow, 'g:clerk', 'clerk'),
        (Allow, 'g:admin', ALL_PERMISSIONS)
    ]

    def __init__(self, request):
        self.request = request
