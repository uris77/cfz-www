# __Installation__

## __Components__

The CFZ App is composed of three main components:

+ __Core__ - contains most of the business logic.
+ __Persistence__ - is concerned with persisting to the database.
+ __WWW__ - exposes the application through a web interface.

Breaking down the apps into components or modules allows us to swap and replace them easier if the need
ever arises. It also enables us to add features or bug fixes on smaller parts and deploy much faster
without needing to redploy the entire application.

## __Deployment Requirements__

- __Python 2.7__ This is standard in most Linux distributions.
- __pip__ Is a tool for installaing and managing python packages. If it is not installed by default, 
we can install it using __easy_install__:

```bash
easy_install pip
```
- __Virtual Env__ Is a Python Virtual Environment Builder [https://pypi.python.org/pypi/virtualenv](https://pypi.python.org/pypi/virtualenv).

```bash
pip install virtualenv
```
- __nginx__ Is an http server optimized for speed [http://wiki.nginx.org/Main](http://wiki.nginx.org/Main).
This can be installed with the default package manager for linux:

```bash
sudo apt-get install nginx
```
- __uwsgi__ Is a stack for deploying and scaling python applications [http://uwsgi-docs.readthedocs.org/en/latest/](http://uwsgi-docs.readthedocs.org/en/latest/).

- __git__ A distributed control versioning system.

- __postgresql__ An advanced relational database management system.

## __Configuring nginx__

__Installation__ won't be covered in this Readme because there is more extensive documentation in nginx 
website [http://wiki.nginx.org/Install](http://wiki.nginx.org/Install). Installing from source or through a package manager will work.

### __Configuration___
The configuration directory will vary depending on where __nginx__ was installed. Normally this would be located under __/etc/nginx/conf__, but for this document we will
refer to it as the configuration folder. This folder will contain a file called __nginx.conf__ where we will put the following configuration:

```bash
user  nginx;
worker_processes  1;
error_log  logs/error.log;
events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout  65;

    server {
        listen       80;
        server_name  cfz.stumblingoncode.com;
        charset utf-8;
        access_log  logs/host.access.log; #  main;
        root /usr/local/code/python/cfz2/cfz-www/cfzwww/static;
        location / {
            uwsgi_pass 127.0.0.1:9001;
            include uwsgi_params;
            uwsgi_param SCRIPT_NAME "";
        }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
   }
}

```
The following attributes will need to be changed to reflect the server configuration:
+ __server_name__ This should reflect the server name.
+ __root__ This is the folder where the static files for the web application. The only thing that will change from this is the path to cfz2:
> /usr/local/code/python/cfz2

## __Virtualenv__

We use __virtualenv__ to isolate the python environment that is running the application from the python environment of the host system. This shields the
application from any updates to the python ecosystem in the host operating system. 

The recommended way for setting up __virtualenv__ is the following:

```bash
$ mkdir /usr/local/code/python
$ cd /usr/local/code/python
$ virtualenv cfz
```
This will create a directory under __/usr/local/code/python__ called __cfz__ , which in turn will hold a __bin__ directory. To run python commands in this virtual 
environment, we need to enable it:

```bash
$ source /usr/local/code/python/cfz/bin/activate
```

More information on __virtualenv__ can be found in its website: [https://pypi.python.org/pypi/virtualenv](https://pypi.python.org/pypi/virtualenv).

## __Configuring uwsgi__

__Installation__ : __uwsgi__ can be installed by following these instructions [http://uwsgi-docs.readthedocs.org/en/latest/WSGIquickstart.html#installing-uwsgi-with-python-support](http://uwsgi-docs.readthedocs.org/en/latest/WSGIquickstart.html#installing-uwsgi-with-python-support).

The simplest way to start a uwsgi instance is through the command line. Here is the example used for this application:

```bash
uwsgi --paste config:/usr/local/code/python/cfz2/cfz-www/production.ini --socket :9001 --processes 8 --threads 4 -d /opt/uwsgi/logs/uwsgi.log -H /usr/local/code/python/cfz2
```
__options__:

+ __config__ The file that contains the settings for the application. This is normally in the www component in a file called __production.ini__ (cfz-www/production.ini).
+ __socket__ The port that the python web application will be listenning to. In this example it is set to port 9001. __nginx__ will forward traffic to this port through port 80.
+ __processess__ The number of processes that will be running. This can be increased or decreased based on the needs. In this case 8 is more than enough.
+ __threads__ The number of threads that will be spawned for each process.
+ __d__ Where we want to store our log file.
+ __H__ The root directory where we are hosting the components for the application. In this example it is hosted under __/usr/local/code/python/cfz2__. This holds the
main components/modules for the application. Here is a quick snapshot of how this directory should look:

```bash
$ ls /usr/local/code/pythonc/cfz2
$ cfz-core cfz-persistence cfz-www
```

To stop the application we would need to get the __pid__ of the processes and send them a __SIGTERM__ :

```bash
$ ps ax | grep uwsgi
15468 ?        Sl     0:01 uwsgi --paste config:/usr/local/code/python/cfz2/cfz-www/production.ini --socket :9001 --processes 8 --threads 4 -d /opt/uwsgi/logs/uwsgi.log -H /usr/local/code/python/cfz2
15475 ?        Sl     0:00 uwsgi --paste config:/usr/local/code/python/cfz2/cfz-www/production.ini --socket :9001 --processes 8 --threads 4 -d /opt/uwsgi/logs/uwsgi.log -H /usr/local/code/python/cfz2
15476 ?        Sl     0:00 uwsgi --paste config:/usr/local/code/python/cfz2/cfz-www/production.ini --socket :9001 --processes 8 --threads 4 -d /opt/uwsgi/logs/uwsgi.log -H /usr/local/code/python/cfz2
15483 ?        Sl     0:00 uwsgi --paste config:/usr/local/code/python/cfz2/cfz-www/production.ini --socket :9001 --processes 8 --threads 4 -d /opt/uwsgi/logs/uwsgi.log -H /usr/local/code/python/cfz2
15484 ?        Sl     0:00 uwsgi --paste config:/usr/local/code/python/cfz2/cfz-www/production.ini --socket :9001 --processes 8 --threads 4 -d /opt/uwsgi/logs/uwsgi.log -H /usr/local/code/python/cfz2
15488 ?        Sl     0:00 uwsgi --paste config:/usr/local/code/python/cfz2/cfz-www/production.ini --socket :9001 --processes 8 --threads 4 -d /opt/uwsgi/logs/uwsgi.log -H /usr/local/code/python/cfz2
15492 ?        Sl     0:00 uwsgi --paste config:/usr/local/code/python/cfz2/cfz-www/production.ini --socket :9001 --processes 8 --threads 4 -d /opt/uwsgi/logs/uwsgi.log -H /usr/local/code/python/cfz2
15496 ?        Sl     0:00 uwsgi --paste config:/usr/local/code/python/cfz2/cfz-www/production.ini --socket :9001 --processes 8 --threads 4 -d /opt/uwsgi/logs/uwsgi.log -H /usr/local/code/python/cfz2

$ kill -9 15468 15475 15476 15483 15484 15488 15492 15496

```

## Postgresql

__Installing__ postgresql is straightforward in a Debian-based system:

```bash
$ sudo apt-get install postgresql-server
```

This will also install a super-user for postgresql. We will need to switch to that user to manage databases:

```bash
$ sudo bash
$ su - postgres
$ createuser cfzadmin
$ createdb --owner=cfzadmin --encoding=UTF8 cfz
$ createdb --owner=cfzadmin --encoding=UTF8 cfz-test
```

We just created a database user called __cfzadmin__ and two databases: __cfz__ & __cfz-test__. The first database is the production database, and the second database
is used to run unit tests to confirm that the code is not broken. 

### Assign User Password.
We need to assign a password to __cfzadmin__. The default password that the application uses is for testing purposes only. It is unsafe and they should be changed.
Changing the password will be discussed later. But for the time being, we need to assign the default password:

```bash
$ sudo bash
$ su - postgres
$ psql -l
> ALTER ROLE cfzadmin WITH PASSWORD 'changeme';
```

### Configure Authentication

Postgresql has different authentication modes, for this application we will use __md5__ mode. This can be configured in __~/data/pg_hba.conf__ :

```bash
$ sudo bash
$ su - postgres
$ vi data/pg_hba.conf
```
Change the file to look similar to:

```postgresql
local   all     all             md5
local   all     127.0.0.1/32    md5
local   all     ::1/128         md5
```

Restart postgresql so that the new settings take effect:

```bash
$ sudo service postgresql restart
```

### Connect to Postgresql
After doing the previous configuration, whenever we connect to postgres, we will be prompted for a password. We will also need to provide
the username and the name of the database we want to connect to. For example, to connect to __cfz-test__ database as __cfzadmin__ user:

```bash
$ psql -Ucfzadmin cfz-test
$ password: 
```


## Git

We need to install and configure git to download the source code for the application. Installing git should be straightforward in a _Debian_ system :

```bash
$ sudo apt-get install git
```

Configuring git consists of telling it our user name and email:

```bash
$ git config --global user.name "John Doe"
$ git config --global user.email "johndoe@mail.com"
```

The email address should be the same as our __[bitbucket](https://bitbucket.org)__ account. It is recommended to have a special account just for downloading the source code
in the event that this responsiblity is given to someone else. After configuring the account, we then need to create a key. __Bitbucket__ has extensive documentation on how 
to do this: [https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git).

## Downloading the Source Code

We need to use git to download the source code from bit bucket. The terminology __git__ uses for this is __checkout__. The generaly command for checking out a 
repository in git is:

```bash
git checkout <name of repo>
```

In this example, we will store all source code under __/usr/local/code/python/cfz__ . This should already have a __virtualenv__ as described above.

```bash
$ cd /usr/local/code/python/cfz
```

The following steps will require that we enable virtualenv. Assuming that virtualenv was installed under __/usr/local/code/python/cfz__:

```bash
$ source /usr/local/code/python/cfz/bin/activate
(cfz)$
```


### CFZ-CORE
The first repository we need to download is __cfz-core__ :

```bash
$ git checkout git@bitbucket.org:uris77/cfz-core.git
```
__Git__ will ask for a password and download the source code after successful authentication. The source code will be located under __/usr/local/code/python/cfz/cfz-core__.

Once the source code is downloaded, we can jump into that directory:

```bash
$ cd cfz-core
```

We need to run the setup file before we are able to run tests. This is only needed the first time the source code is downloaded. It installs any 
dependencies required for __cfz-core__:

```bash
$ python setup.py develop
```

__cfz-core__ has a test suite that can be used to verify that there are no regressions or bugs introduced. It is recommended to run the tests everytime we updated the 
source code:

```bash
$ nostests
...........................................................................................................
----------------------------------------------------------------------
Ran 107 tests in 0.683s

OK
$
```

Now we can deploy this module. We can do this in 2 ways: deploy it as source code or install it as binary. For production, we an forego the first option and just
install the module:

```bash
$ python setup.py install
```

### CFZ-PERSISTENCE
This module contains the tools required for persisting data.

```bash
$ git checkout git@bitbucket.org:uris77/cfz-persistence.git
```

After authenticating, the source code will be downloaded to __/usr/local/code/python/cfz/cfz-persistence__.

### CFZ-WWW
This module exposes the application through a web interface.

```bash
$ git checkout git@bitbucket.org:uris77/cfz-www.git
```

After authenticating, the source code will be downloaded to __/usr/local/code/python/cfz/cfz-www__.
